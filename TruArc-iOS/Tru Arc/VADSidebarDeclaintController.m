//
//  VADSidebarDeclaintController.m
//  Tru Arc
//
//  Created by MAC3 on 12/24/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSidebarDeclaintController.h"

#import "VADGlobal.h"

@interface VADSidebarDeclaintController ()

- (void) loadPreviouseInfo;

@end

@implementation VADSidebarDeclaintController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self loadPreviouseInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadPreviouseInfo {
    BOOL bAuto = [[[VADGlobal getInstance] loadInfo:DECLAINT_AUTO] boolValue];
    
    [_stAuto setOn:bAuto];
    
    if ( bAuto ) {
        [_tfEast setEnabled:NO];
        [_tfWest setEnabled:NO];
        
        [_tfEast setTextColor:[UIColor grayColor]];
        [_tfWest setTextColor:[UIColor grayColor]];
        
        [_tfEast resignFirstResponder];
        [_tfWest resignFirstResponder];
    }else {
        [_tfEast setEnabled:YES];
        [_tfWest setEnabled:YES];
        
        [_tfEast setTextColor:[UIColor whiteColor]];
        [_tfWest setTextColor:[UIColor whiteColor]];
    }
    
    _tfEast.text = [[VADGlobal getInstance] loadInfo:DEC_EAST];
    _tfWest.text = [[VADGlobal getInstance] loadInfo:DEC_WEST];

}
   
- (IBAction)didChangeOption:(id)sender {
    
    UISwitch* switchCtrl = (UISwitch*)sender;
    
    [[VADGlobal getInstance] saveInfo:switchCtrl.isOn ? @"1": @"0" :DECLAINT_AUTO];
    
    [self loadPreviouseInfo]; 
}

 
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ( textField == _tfEast ) {
        [[VADGlobal getInstance] saveInfo:_tfEast.text :DEC_EAST];
    }else {
        [[VADGlobal getInstance] saveInfo:_tfWest.text :DEC_WEST];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ( textField == _tfEast ) {
        [_tfWest setText:@""];
        [[VADGlobal getInstance] saveInfo:@"" :DEC_WEST];
    }else {
        [_tfEast setText:@""];
        [[VADGlobal getInstance] saveInfo:@"" :DEC_EAST];
    }
    
    return YES;
}

@end
