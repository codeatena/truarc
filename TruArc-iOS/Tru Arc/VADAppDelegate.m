//
//  VADAppDelegate.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADAppDelegate.h"

#import "VADGlobal.h"

@implementation VADAppDelegate

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    if (! ([CLLocationManager locationServicesEnabled]) || ( [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)) {
        NSLog(@"location service disabled");

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Disabled"
                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else {
        NSLog(@"location service enabled");
    }
    
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInt:UNITS_IMPERIAL], @"compass.target.units",
                                 [NSNumber numberWithBool:YES], @"compass.showLandmarks",
                                 [NSNumber numberWithBool:YES], @"map.showLandmarks",
                                 [NSNumber numberWithBool:YES], @"map.showCompass",
                                 nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    
    NSError *error;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
        /* Delete Previous Landmark Items */
    NSFetchRequest *deleteRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Landmark" inManagedObjectContext:context];
    [deleteRequest setEntity:entity];
    [deleteRequest setReturnsObjectsAsFaults:YES];
    for (NSManagedObject *landmarkItem in [context executeFetchRequest:deleteRequest error:&error]){
        [context deleteObject:landmarkItem];
    }
    
    deleteRequest = [[NSFetchRequest alloc] init];
    entity = [NSEntityDescription entityForName:@"Route" inManagedObjectContext:context];
    [deleteRequest setEntity:entity];
    [deleteRequest setReturnsObjectsAsFaults:NO];
    for (VADRoute *route in [context executeFetchRequest:deleteRequest error:&error]){
        [context deleteObject:route];
    }
    
    deleteRequest = [[NSFetchRequest alloc] init];
    entity = [NSEntityDescription entityForName:@"EducationVideo" inManagedObjectContext:context];
    [deleteRequest setEntity:entity];
    [deleteRequest setReturnsObjectsAsFaults:NO];
    for (VADEducationVideo *educationItem in [context executeFetchRequest:deleteRequest error:&error]){
        [[NSFileManager defaultManager] removeItemAtPath:[educationItem localVideoPath] error:&error];
    }

    self.route = [NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:context];
    [_route setValue:@"Royal Arch" forKey:@"title"];

    if ( ![[[VADGlobal getInstance] loadInfo:INIT_LOADING] boolValue] ) {
        
        [[VADGlobal getInstance] saveInfo:@"1" :INIT_LOADING];
        [[VADGlobal getInstance] saveInfo:@"1" :DECLAINT_AUTO];
        [[VADGlobal getInstance] saveInfo:@"0" :SETTING_MODE];
        
        self.arrWayPoints = [[NSMutableArray alloc] init];
        
        [self saveWayPoints];
        
    }else {
        [self loadWayPoints];
    }
    
//    [self initWayPoint];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

+ (VADAppDelegate*)sharedDelegate {
    return (VADAppDelegate*)[UIApplication sharedApplication].delegate;
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Tru Arc.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - WayPoint insert, delete, save, load
- (void) insertNewWayPoint:(CLLocationCoordinate2D) coordinate {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    VADLandmark *landmarkG = [NSEntityDescription insertNewObjectForEntityForName:@"Landmark" inManagedObjectContext:context];
    [landmarkG setValue:@"WayPoint" forKey:@"title"];
    [landmarkG setValue:[NSNumber numberWithDouble:coordinate.latitude] forKey:@"latitude"];
    [landmarkG setValue:[NSNumber numberWithDouble:coordinate.longitude] forKey:@"longitude"];
    [landmarkG setValue:[NSDate date] forKey:@"created"];

    [_route addWaypointsObject:landmarkG];
    
    [self saveWayPoints];
}

- (void) removeWayPoint:(VADLandmark*)obj {
    
    int nIndex = 0;
    for ( VADLandmark* landmark in [_route.waypoints array] ){
        if ([landmark isSameObject:obj] ){
            break;
        }
        
        nIndex ++;
    }
    
    if ( nIndex < [_route.waypoints.array count] )
        [_route removeObjectFromWaypointsAtIndex:nIndex];
    
    [self saveWayPoints];
}

- (void) updateWayPointTitle:(VADLandmark*)obj :(NSString*)newTitle {
    for ( VADLandmark* landmark in [_route.waypoints array] ){
        if ([landmark isSameObject:obj] ){
            [landmark setValue:newTitle forKey:@"title"];
            break;
        }
    }   
    
    [self saveWayPoints];
}

- (NSString*) getPath {
    
    NSString* strFilePath;
    
    NSString* fileaname = @"waypoint.dat";
    
#if TARGET_IPHONE_SIMULATOR
    strFilePath = [NSString stringWithFormat:@"%@/%@", [[[[NSString stringWithUTF8String:__FILE__] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent], fileaname ];
#else
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    if ( !documentsDirectory )
        return nil;
    
    strFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileaname];
#endif
    
    return strFilePath;
}

- (void) removeWayPointAtIndex:(int)index {

    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePins" object:@"1"];
    
    int i = 0;
    for (VADLandmark *landmark in _route.waypoints.array) {
        
        if ( i == index ){
            [_route removeObjectFromWaypointsAtIndex:index];
            break;
        }
        
        i ++;
    }
    
    [self saveWayPoints];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePins" object:@"2"];
}

- (void) saveWayPoints {
    
    [_arrWayPoints removeAllObjects];
    
    for (VADLandmark *landmark in _route.waypoints.array ){
        NSDictionary* dicWayPoint = [NSDictionary dictionaryWithObjectsAndKeys:  landmark.title, @"title",
                                     landmark.latitude, @"latitude",
                                     landmark.longitude, @"longitude",
                                     landmark.created, @"created"
                                     ,nil];
        [_arrWayPoints addObject:dicWayPoint];
    }
    
    NSString* path = [self getPath];
    BOOL bSaved = [_arrWayPoints writeToFile:path atomically:YES];
    
    if ( !bSaved )
        NSLog(@"save failed");
    else
        NSLog(@"save successed");
}

- (void) loadWayPoints {
    NSString* path = [self getPath];
    
    BOOL bExitFile = [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    if ( !bExitFile )
        NSLog(@"not exited file");
    
    self.arrWayPoints = [NSMutableArray arrayWithContentsOfFile:path];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for ( NSDictionary* dic in _arrWayPoints ){
        VADLandmark *landmarkG = [NSEntityDescription insertNewObjectForEntityForName:@"Landmark" inManagedObjectContext:context];
        [landmarkG setValue:[dic valueForKey:@"title"] forKey:@"title"];
        [landmarkG setValue:[dic valueForKey:@"latitude"] forKey:@"latitude"];
        [landmarkG setValue:[dic valueForKey:@"longitude"] forKey:@"longitude"];
        [landmarkG setValue:[dic valueForKey:@"created"] forKey:@"created"];
        
        [_route addWaypointsObject:landmarkG];
    }
}

- (void) initWayPoint
{
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute.waypoints"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Route"];
    [request setReturnsObjectsAsFaults:YES];
    for (VADRoute *route in [managedObjectContext executeFetchRequest:request error:nil]){
        [[VADNavigationData instance] setCurrentRoute:route];
    }
    
    BOOL bTargetSelected = [[[VADGlobal getInstance] loadInfo:IS_TARGET_SET] boolValue];
    int nSelectIndex;
    if ( bTargetSelected )
        nSelectIndex = [[[VADGlobal getInstance] loadInfo:ACTIVE_TARGET] intValue];
    else
        nSelectIndex = -1;

}

@end
