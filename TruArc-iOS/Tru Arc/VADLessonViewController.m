//
//  VADLessonViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/6/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADLessonViewController.h"

@interface VADLessonViewController ()

@end

@implementation VADLessonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [self.scrollView addSubview:self.imageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setEducationImage:(UIImage *)newEducationImage
{
    self.imageView = [[UIImageView alloc] initWithImage:newEducationImage];
}

- (IBAction)didTapLogo:(UIButton *)sender {
    [self.revealViewController revealToggle:sender];
}

-(void)viewWillLayoutSubviews
{
}

-(void)viewDidLayoutSubviews
{
    if (self.imageView.image){
        double smallSide = MIN(CGRectGetWidth(self.scrollView.bounds),CGRectGetHeight(self.scrollView.bounds));
        double scale = smallSide / self.imageView.image.size.width;
        self.imageView.frame = CGRectMake(CGRectGetWidth(self.scrollView.bounds)/2 - (smallSide / 2), 0, smallSide, self.imageView.image.size.height * scale);
        self.scrollView.contentSize = self.imageView.frame.size;
        self.scrollView.zoomScale = 1.0;
    }
}

@end
