//
//  VADCompassViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/25/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#define kLwBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "VADCameraView.h"
#import "SWRevealViewController.h"
#import "VADAppDelegate.h"
#import "VADCompassPanelView.h"
#import "VADAngleMeterView.h"
#import "VADHorizonMeterView.h"
#import "VADSensorManager.h"
#import "VADSidebarViewController.h"

@interface VADCompassViewController : UIViewController <CLLocationManagerDelegate, SWRevealViewControllerDelegate>
{
    UIImageView *northImageView;
    CLLocationCoordinate2D currentCoordinate;
    
    UIImageView *arrowImageView;
    
    double curArrow;
//    UILabel * testLabel;
}

    /* View Outlets */
@property (strong, nonatomic) IBOutlet UIButton *btnScreenshot;

@property (strong, nonatomic) IBOutlet UIView *viewCoordinates;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnstLandscapeToolbar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnstPortaitToolbar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnstLandscapeLeft;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnstPortraitBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblLCoord;

@property (strong, nonatomic) IBOutlet UILabel *lblHorizon;
@property (strong, nonatomic) IBOutlet UILabel *lblElevation;
@property (strong, nonatomic) IBOutlet UILabel *lblBearing;

@property (strong, nonatomic) IBOutlet VADCompassPanelView *viewLandscapePanel;
@property (strong, nonatomic) IBOutlet VADCompassPanelView *viewPortraitPanel;
@property (strong, nonatomic) IBOutlet UIView *viewTools;

@property (strong, nonatomic) IBOutlet UIView *viewInstruments;
@property (strong, nonatomic) IBOutlet UIView *viewConventional;
@property (strong, nonatomic) IBOutlet VADAngleMeterView *viewElevation;
@property (strong, nonatomic) IBOutlet VADAngleMeterView *viewHorizon;
@property (strong, nonatomic) IBOutlet VADHorizonMeterView *viewHorizonMeter;

@property (nonatomic) BOOL bCaptureSuccess;

    /* Action Methods */
- (IBAction)tapScreenshot:(UIButton *)sender;
- (IBAction)tapLogo:(UIButton *)sender;
- (IBAction)tapTools:(UIButton *)sender;

    /* Camera Properties */
@property VADCameraView* cameraBackgroundView;
@property float cameraCompensationAngle;

    /* Kalman Filter Values */
@property double lastYaw;
@property double lastRoll;
@property double lastPitch;
@property double lastHorizon;

    /* Clock Timer */
@property NSTimer* tmrClock;

@property (assign) BOOL orientationIsFaceUp;

@end
