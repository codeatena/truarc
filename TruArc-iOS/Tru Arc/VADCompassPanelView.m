//
//  VADCompassPanelView.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/3/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADCompassPanelView.h"

@interface VADCompassPanelView(){
    
}

@end

@implementation VADCompassPanelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        self.layer.mask = [self makePartialMask];
        
    }
    return self;
}

- (CALayer*)makePartialMask
{
    CGSize size = self.bounds.size;
    
    CAShapeLayer* maskLayer;
    CGMutablePathRef mask = CGPathCreateMutable();
    CGPathMoveToPoint(mask, NULL, 0.0, 0.0);
    CGPathAddLineToPoint(mask, NULL, SIZE_HEADINGBOXWIDTH - (size.height - SIZE_LANDSCAPEHEIGHT), 0.0);
    CGPathAddLineToPoint(mask, NULL, SIZE_HEADINGBOXWIDTH, (size.height - SIZE_LANDSCAPEHEIGHT));
    CGPathAddLineToPoint(mask, NULL, SIZE_HEADINGBOXWIDTH, size.height);
    CGPathAddLineToPoint(mask, NULL, 0.0, size.height);
    CGPathAddLineToPoint(mask, NULL, 0.0, 0.0);
    maskLayer = [CAShapeLayer layer];
    maskLayer.path = mask;
    return maskLayer;
}

-(CALayer*)makeFullMask
{
    CGSize size = self.bounds.size;
    
    CAShapeLayer* maskLayer;
    CGMutablePathRef mask = CGPathCreateMutable();
    CGPathMoveToPoint(mask, NULL, 0.0, 0.0);
    CGPathAddLineToPoint(mask, NULL, SIZE_HEADINGBOXWIDTH - (size.height - SIZE_LANDSCAPEHEIGHT), 0.0);
    CGPathAddLineToPoint(mask, NULL, SIZE_HEADINGBOXWIDTH, (size.height - SIZE_LANDSCAPEHEIGHT));
    CGPathAddLineToPoint(mask, NULL, size.width * 2, size.height - SIZE_LANDSCAPEHEIGHT);
    CGPathAddLineToPoint(mask, NULL, size.width * 2, size.height);
    CGPathAddLineToPoint(mask, NULL, 0.0, size.height);
    CGPathAddLineToPoint(mask, NULL, 0.0, 0.0);
    maskLayer = [CAShapeLayer layer];
    maskLayer.path = mask;
    
    return maskLayer;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect viewBounds = self.bounds;
    
    CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
    CGContextSetFillColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
    
    
    if (!self.isPortrait){
        
        [self.txtHeading drawInRect:CGRectMake(0, 0, SIZE_HEADINGWIDTH, viewBounds.size.height) withFont:FONT_HEADING lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentRight];
        [@"°" drawInRect:CGRectMake(SIZE_HEADINGWIDTH, 0, SIZE_HEADINGBOXWIDTH - SIZE_HEADINGWIDTH, viewBounds.size.height) withFont:FONT_HEADING lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentLeft];
        
        [self.txtDirection drawInRect:CGRectMake(SIZE_HEADINGWIDTH, viewBounds.size.height - SIZE_LANDSCAPEHEIGHT + 2, SIZE_HEADINGBOXWIDTH - SIZE_HEADINGWIDTH, SIZE_LANDSCAPEHEIGHT) withFont:FONT_DIRECTION lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentCenter];
        
        double toolY = viewBounds.size.height - SIZE_LANDSCAPEHEIGHT;
        double toolWidth = (viewBounds.size.width - SIZE_HEADINGBOXWIDTH) / 3.0;
        CGSize labelSize, dataSize;
        CGRect toolRect;
            /* Render Target */
        toolRect = CGRectMake(SIZE_HEADINGBOXWIDTH, toolY, toolWidth, SIZE_LANDSCAPEHEIGHT);
        labelSize = [@"TARGET" sizeWithFont:FONT_LABELS];
        //NSLog(@"target = %@", self.txtTarget);

        dataSize = [self.txtTarget sizeWithFont:FONT_DATA];
        [@"TARGET" drawInRect:CGRectMake(toolRect.origin.x, toolRect.origin.y + (toolRect.size.height - labelSize.height) / 2, labelSize.width, labelSize.height) withFont:FONT_LABELS lineBreakMode:NSLineBreakByClipping];
        [self.txtTarget drawInRect:CGRectMake(toolRect.origin.x + labelSize.width + 4, toolRect.origin.y + (toolRect.size.height - dataSize.height) / 2, toolRect.size.width - labelSize.width - 4, dataSize.height)withFont:FONT_DATA lineBreakMode:NSLineBreakByClipping];
        
            /* Render Time */
        toolRect = CGRectMake(SIZE_HEADINGBOXWIDTH + toolWidth, toolY, toolWidth, SIZE_LANDSCAPEHEIGHT);
        labelSize = [@"TIME" sizeWithFont:FONT_LABELS];
        dataSize = [self.txtTime sizeWithFont:FONT_DATA];
        [@"TIME" drawInRect:CGRectMake(toolRect.origin.x, toolRect.origin.y + (toolRect.size.height - labelSize.height) / 2, labelSize.width, labelSize.height) withFont:FONT_LABELS lineBreakMode:NSLineBreakByClipping];
        [self.txtTime drawInRect:CGRectMake(toolRect.origin.x + labelSize.width + 4, toolRect.origin.y + (toolRect.size.height - dataSize.height) / 2, toolRect.size.width - labelSize.width - 4, dataSize.height)withFont:FONT_DATA lineBreakMode:NSLineBreakByClipping];
        
            /* Render Altitude */
        toolRect = CGRectMake(SIZE_HEADINGBOXWIDTH + (toolWidth * 2), toolY, toolWidth, SIZE_LANDSCAPEHEIGHT);
        labelSize = [@"ALTITUDE" sizeWithFont:FONT_LABELS];
        dataSize = [self.txtAltitude sizeWithFont:FONT_DATA];
        [@"ALTITUDE" drawInRect:CGRectMake(toolRect.origin.x, toolRect.origin.y + (toolRect.size.height - labelSize.height) / 2, labelSize.width, labelSize.height) withFont:FONT_LABELS lineBreakMode:NSLineBreakByClipping];
        [self.txtAltitude drawInRect:CGRectMake(toolRect.origin.x + labelSize.width + 4, toolRect.origin.y + (toolRect.size.height - dataSize.height) / 2, toolRect.size.width - labelSize.width - 4, dataSize.height)withFont:FONT_DATA lineBreakMode:NSLineBreakByClipping];
        
    } else {
        
        [[self.txtHeading stringByAppendingString:@"°"] drawInRect:CGRectMake(0, 0, viewBounds.size.width / 2, viewBounds.size.height - SIZE_P_COORDHEIGHT) withFont:FONT_P_HEADING lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentRight];
        
        [self.txtLatitude drawInRect:CGRectMake(0, viewBounds.size.height - SIZE_P_COORDHEIGHT, viewBounds.size.width / 2, SIZE_P_COORDHEIGHT) withFont:FONT_P_COORDINATES lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentCenter];
        [self.txtLongitude drawInRect:CGRectMake(viewBounds.size.width / 2, viewBounds.size.height - SIZE_P_COORDHEIGHT, viewBounds.size.width / 2, SIZE_P_COORDHEIGHT) withFont:FONT_P_COORDINATES lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentCenter];
        
        [self.txtDirection drawInRect:CGRectMake(viewBounds.size.width / 2, 4, 40, 40) withFont:FONT_P_DIRECTION lineBreakMode:NSLineBreakByClipping];
        
        [@"TIME" drawInRect:CGRectMake(viewBounds.size.width / 2 + 40, 2, (viewBounds.size.width / 2 - 40) / 2, SIZE_P_LABELS) withFont:FONT_P_LABELS lineBreakMode:NSLineBreakByClipping];
        [self.txtTime drawInRect:CGRectMake(viewBounds.size.width / 2 + 40, SIZE_P_LABELS, (viewBounds.size.width / 2 - 40) / 2, SIZE_P_DATA) withFont:FONT_P_DATA lineBreakMode:NSLineBreakByClipping];
        
        
        [@"ALTITUDE" drawInRect:CGRectMake(viewBounds.size.width / 2 + 40 + (viewBounds.size.width / 2 - 40) / 2, 2, (viewBounds.size.width / 2 - 40) / 2, SIZE_P_LABELS) withFont:FONT_P_LABELS lineBreakMode:NSLineBreakByClipping];
        [self.txtAltitude drawInRect:CGRectMake(viewBounds.size.width / 2 + 40 + (viewBounds.size.width / 2 - 40) / 2, SIZE_P_LABELS, (viewBounds.size.width / 2 - 40) / 2, SIZE_P_DATA) withFont:FONT_P_DATA lineBreakMode:NSLineBreakByClipping];
        
        [@"TARGET" drawInRect:CGRectMake(viewBounds.size.width / 2, viewBounds.size.height - SIZE_P_COORDHEIGHT - SIZE_P_TARGETDATA - SIZE_P_TARGETLABEL, viewBounds.size.width / 2, SIZE_P_TARGETLABEL) withFont:FONT_P_TARGETLABEL lineBreakMode:NSLineBreakByClipping];
        [self.txtTarget drawInRect:CGRectMake(viewBounds.size.width / 2, viewBounds.size.height - SIZE_P_COORDHEIGHT - SIZE_P_TARGETDATA, viewBounds.size.width / 2, SIZE_P_TARGETDATA) withFont:FONT_P_TARGETDATA lineBreakMode:NSLineBreakByClipping];
    }
}

- (void)setHeading:(CLLocationDirection)newHeading
{
    self.txtHeading = [NSString stringWithFormat:@"%.0f", newHeading];
    
    if(newHeading > 23 && newHeading <= 67){
        self.txtDirection = @"NE";
    } else if(newHeading <= 112){
        self.txtDirection = @"E";
    } else if(newHeading <= 167){
        self.txtDirection = @"SE";
    } else if(newHeading <= 202){
        self.txtDirection = @"S";
    } else if(newHeading <= 247){
        self.txtDirection = @"SW";
    } else if(newHeading <= 293){
        self.txtDirection = @"W";
    } else if(newHeading <= 337){
        self.txtDirection = @"NW";
    } else {
        self.txtDirection = @"N";
    }
    [self setNeedsDisplay];
}

- (void)errHeading
{
    self.txtHeading = @"ERR";
    self.txtDirection = @" --- ";
    [self setNeedsDisplay];
}

- (void)setTarget:(NSString *)newTarget
{
    self.txtTarget = newTarget;
    //NSLog(@"%@", self.txtTarget);
    [self setNeedsDisplay];
}

- (void)setTime:(NSString *)newTime
{
    self.txtTime = newTime;
    [self setNeedsDisplay];
}

- (void)setAltitude:(NSString *)newAltitude
{
    self.txtAltitude = newAltitude;
    [self setNeedsDisplay];
}

- (void)setPortrait:(BOOL)newIsPortrait
{
    self.isPortrait = newIsPortrait;
    if (self.isPortrait) self.layer.mask = nil;
}

- (void)setLatitude:(NSString *)newLatitude
{
    self.txtLatitude = newLatitude;
    [self setNeedsDisplay];
}

- (void)setLongitude:(NSString *)newLongitude
{
    self.txtLongitude = newLongitude;
    [self setNeedsDisplay];
}

- (void)setMinimal:(BOOL)newIsMinimal
{
    self.isMinimal = newIsMinimal;
    if (self.isMinimal && !self.isPortrait){
        self.layer.mask = [self makePartialMask];
    } else if (!self.isMinimal && !self.isPortrait){
        self.layer.mask = [self makeFullMask];
    }
}

@end
