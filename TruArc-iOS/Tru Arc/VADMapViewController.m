//
//  VADMapViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/3/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADMapViewController.h"

#import "VADAppDelegate.h"

@interface VADMapViewController ()

@property (nonatomic, weak) VADRoute * currentRoute;
@property (assign) BOOL ignoreWaypointChanges;

- (void) loadPinsWithScale:(BOOL)bScale;

@end

@implementation VADMapViewController

@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
        /* Attach Sidebar Pan Gesture Recognizer */
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController setDelegate:self];
    
        /* Register As A Map Delegate */
    [self.mkMapView setDelegate:self];
    
    self.managedObjectContext = [[VADAppDelegate sharedDelegate] managedObjectContext];
    
    [self loadPinsWithScale:YES];
    
        /* Zoom In On User Location */
    [self.mkMapView setUserTrackingMode:MKUserTrackingModeFollow];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePin:) name:@"updatePins" object:nil];
}

- (void) updatePin:(NSNotification*)notification {
    
    if ( [notification.object isEqualToString:@"1"] ) {
        NSArray* annotations = self.mkMapView.annotations;
        [self.mkMapView removeAnnotations:annotations];
    }else {
        [self loadPinsWithScale:NO];
    }
}

- (void) loadPinsWithScale:(BOOL)bScale {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Route"];
    [request setReturnsObjectsAsFaults:YES];
    for (VADRoute *route in [self.managedObjectContext executeFetchRequest:request error:nil]){
        
        self.currentRoute = route;
        if (self.currentRoute == nil)
            return;
        
        [self.mkMapView removeAnnotations:[self.currentRoute.waypoints array]];
        [self.mkMapView addAnnotations:[self.currentRoute.waypoints array]];
        
        if ( [self.currentRoute.waypoints count] > 0 && bScale )
            [self.mkMapView setRegion:[self.currentRoute getCoordinateRegion] animated:YES];
    }
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    if ([keyPath isEqual:@"currentRoute"]){
//        self.currentRoute = [change objectForKey:NSKeyValueChangeNewKey];
//        if (self.currentRoute == nil) return;
//        [self.mkMapView removeAnnotations:[self.currentRoute.waypoints array]];
//        [self.mkMapView addAnnotations:[self.currentRoute.waypoints array]];
//        
//        [self.mkMapView setRegion:[self.currentRoute getCoordinateRegion] animated:YES];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapLogoButton:(UIButton *)sender {
    [self.revealViewController revealToggle:sender];
}

#pragma mark - UILongPressGestureRecognizerDelegate

- (IBAction)didRecognizeLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mkMapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mkMapView convertPoint:touchPoint toCoordinateFromView:self.mkMapView];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    VADLandmark *landmarkG = [NSEntityDescription insertNewObjectForEntityForName:@"Landmark" inManagedObjectContext:context];
    [landmarkG setValue:@"WayPoint" forKey:@"title"];
    [landmarkG setValue:[NSNumber numberWithDouble:touchMapCoordinate.latitude] forKey:@"latitude"];
    [landmarkG setValue:[NSNumber numberWithDouble:touchMapCoordinate.longitude] forKey:@"longitude"];
    [landmarkG setValue:[NSDate date] forKey:@"created"];
    
//    [VADNavigationData instance].currentTarget = touchMapCoordinate;

    CLLocationDistance distance = [self.mkMapView.userLocation.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:touchMapCoordinate.latitude longitude:touchMapCoordinate.longitude]];
    
    if ( distance > 0 )
        [self.lblDistance setText:[NSString stringWithFormat:@"%@ %@", [VADFormatTool formatDistance:distance withUnit:UNITS_IMPERIAL], [VADFormatTool formatDistance:distance withUnit:UNITS_METRIC]]];
    
    [self.mkMapView addAnnotation:landmarkG];
    
    [[VADAppDelegate sharedDelegate] insertNewWayPoint:touchMapCoordinate];
}

#pragma mark - SWRevealViewControllerDelegate

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft){
        [self.mkMapView setUserInteractionEnabled:YES];
        [self.mkMapView setScrollEnabled:YES];
        [self.mkMapView setZoomEnabled:YES];
    } else{
        [self.mkMapView setUserInteractionEnabled:NO];
        [self.mkMapView setScrollEnabled:NO];
        [self.mkMapView setZoomEnabled:NO];
    }
}

#pragma mark - MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    if([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineView *lineView = [[MKPolylineView alloc] initWithPolyline:overlay];
        lineView.lineWidth = 15;
        lineView.strokeColor = [UIColor blueColor];
        lineView.fillColor = [UIColor cyanColor];
        lineView.alpha = 0.5;
        
        return lineView;
    }
    return nil;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation
{
    /* Ignore User Location Annotation */
    if ([annotation isKindOfClass:[MKUserLocation class]]) return nil;
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"MyPin"];
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MyPin"];
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.pinColor = MKPinAnnotationColorRed;
    }
    
    annotationView.annotation = annotation;
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    self.selectedLandMark = (VADLandmark*)view.annotation;

    
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Change waypoint name"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeAlphabet;
    [alert textFieldAtIndex:0].keyboardAppearance = UIKeyboardAppearanceAlert;
    [alert textFieldAtIndex:0].text = _selectedLandMark.title;
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [alert setTransform:moveUp];
    
    [alert show];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ( buttonIndex == 1 ) {
        
        NSString* newTitle = [alertView textFieldAtIndex:0].text;
        
        if ( [newTitle isEqualToString:@""]) {
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                            message:@"please input waypoint title"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        [[VADAppDelegate sharedDelegate] updateWayPointTitle:_selectedLandMark :newTitle];
        [_selectedLandMark setValue:newTitle forKey:@"title"];
        _selectedLandMark = nil;
    }
}

@end
