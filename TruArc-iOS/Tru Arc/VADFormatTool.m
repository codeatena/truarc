//
//  VADFormatTool.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/4/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADFormatTool.h"



@implementation VADFormatTool


+ (NSString*) formatDistance:(CLLocationDistance)distance withUnit:(int)unit
{
    return [VADFormatTool formatDistance:distance withUnit:unit withPrecision:3];
}

+ (NSString*) formatDistance:(CLLocationDistance)distance withUnit:(int)unit withPrecision:(int)precision
{
    NSString *strUnit;
    switch(unit){
        case UNITS_METRIC:
            strUnit = @"m";
            if (distance >= METERS_PER_KILOMETER){
                distance = distance / METERS_PER_KILOMETER;
                strUnit = @"km";
            }
            break;
        case UNITS_IMPERIAL:
        default:
            strUnit = @"ft";
            distance = distance * FEET_PER_METER;   // Convert To Feet
            if (distance >= FEET_PER_MILE){
                distance = distance / FEET_PER_MILE;
                strUnit = @"Mi";
            }
            break;
    }
    return [NSString stringWithFormat:@"%1.*f%@", (int)MAX(4 - log10(distance), 0) , distance, strUnit];
}

@end
