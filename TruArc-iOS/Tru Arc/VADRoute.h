//
//  VADRoute.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@class VADLandmark;

@interface VADRoute : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSOrderedSet *waypoints;
@end

@interface VADRoute (CoreDataGeneratedAccessors)

- (void)insertObject:(VADLandmark *)value inWaypointsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromWaypointsAtIndex:(NSUInteger)idx;
- (void)insertWaypoints:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeWaypointsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInWaypointsAtIndex:(NSUInteger)idx withObject:(VADLandmark *)value;
- (void)replaceWaypointsAtIndexes:(NSIndexSet *)indexes withWaypoints:(NSArray *)values;
- (void)addWaypointsObject:(VADLandmark *)value;
- (void)removeWaypointsObject:(VADLandmark *)value;
- (void)addWaypoints:(NSOrderedSet *)values;
- (void)removeWaypoints:(NSOrderedSet *)values;

- (MKPolyline*) makePolylineOverlay;
- (MKCoordinateRegion) getCoordinateRegion;

@end
