//
//  VADCameraView.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADCameraView.h"

@interface VADCameraView ()

@property (nonatomic) dispatch_queue_t sessionQueue;
@property (retain) AVCaptureSession *captureSession;

@end

@implementation VADCameraView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
            /** Initialize an Audio/Video Capture Session **/
        [self setCaptureSession:[[AVCaptureSession alloc] init]];
        
            /** Create A Preview Layer To Attach To The View **/
        [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:[self captureSession]]];
            /** Attach Preview Layer To The View **/
        CGRect layerRect = frame;
        [[self previewLayer] setBounds:layerRect];
        [[self previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];
        [[self layer] addSublayer:[self previewLayer]];
        
        dispatch_queue_t sessionQueue = dispatch_queue_create("camera.captureSession.queue", DISPATCH_QUEUE_SERIAL);
        [self setSessionQueue:sessionQueue];
        
        dispatch_async(sessionQueue, ^{
            [self.captureSession setSessionPreset:AVCaptureSessionPreset640x480];
            
                /** Load A List Of Media Devices **/
            NSArray *devices = [AVCaptureDevice devices];
            AVCaptureDevice *camera;
            
                /** Find The Back Facing Camera **/
            for (AVCaptureDevice *device in devices) {
                
                if ([device hasMediaType:AVMediaTypeVideo]){
                    
                    if ([device position] == AVCaptureDevicePositionBack){
                        
                        camera = device;
                        break;
                    }
                }
            }
            
            
            [[self previewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
                /** Attach Back Facing Camera To Capture Session **/
            NSError *error = nil;
            AVCaptureDeviceInput *cameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:camera error:&error];
            
            if (!error){
                
                if ([[self captureSession] canAddInput:cameraDeviceInput]){
                    [[self captureSession] addInput:cameraDeviceInput];
                } else {
//                    NSLog(@"Failed To Add Back Facing Camera Input");
                }
                
            }
            
            [self setStillImageOutput:[[AVCaptureStillImageOutput alloc] init]];
            NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            AVVideoCodecJPEG, AVVideoCodecKey,
                                            nil];
            [self.stillImageOutput setOutputSettings:outputSettings];
            if ([[self captureSession] canAddOutput:self.stillImageOutput])
                [[self captureSession] addOutput: self.stillImageOutput];
            
                /** Start Capture Session **/
            [[self captureSession] startRunning];
//            NSLog(@"Camera Capture Started");
        });
    }
    return self;
}

- (void)updateOrientation:(UIInterfaceOrientation)orientation
{
    dispatch_async([self sessionQueue], ^{
//        NSLog(@"Updating Orientation of Camera");
        if ( [self previewLayer].connection.supportsVideoOrientation )
            [self previewLayer].connection.videoOrientation = orientation;
    });
}

- (void)capture:(void(^)(UIImage* image)) callback
{
    dispatch_async([self sessionQueue], ^{
        AVCaptureConnection *stillImageConnection = nil;
        
    //    [stillImageConnection setVideoScaleAndCropFactor:[self previewLayer].connection.videoScaleAndCropFactor];
        
        for ( AVCaptureConnection* connection in [[self stillImageOutput] connections] ){
            for ( AVCaptureInputPort* port in [connection inputPorts]) {
                if ( [[port mediaType] isEqual:AVMediaTypeVideo]) {
                    stillImageConnection = connection;
                    break;
                }
            }
            
            break;
        }
        
        if ( stillImageConnection == nil ) {
            callback(nil);
            return;
        }
        
        if ([stillImageConnection isVideoOrientationSupported])
            [stillImageConnection setVideoOrientation:[self previewLayer].connection.videoOrientation];

        [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:stillImageConnection
             completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                 
                 if (imageDataSampleBuffer != NULL) {
                     NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                     
                     UIImage *image = [[UIImage alloc] initWithData:imageData];
                     callback(image);
                 } else
                     callback(nil);
             }];
    });
}

- (void)layoutSubviews
{
        /* The Video Frame SHOULD NOT Be Animated */
    [CATransaction begin];
    [CATransaction setAnimationDuration:0];
    [CATransaction setDisableActions:YES];
    
    CGRect layerRect = [[self layer] bounds];
    [[self previewLayer] setBounds:layerRect];
    [[self previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];
    
    [CATransaction commit];
}

- (void)startCamera
{
    dispatch_async([self sessionQueue], ^{
        [[self captureSession] startRunning];
    });
}

- (void)stopCamera
{
    dispatch_async([self sessionQueue], ^{
        [[self captureSession] stopRunning];
    });
}

- (void)dealloc
{
    [[self captureSession] stopRunning];
//    NSLog(@"Camera Capture Stopped");
}

@end
