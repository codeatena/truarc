//
//  VADNavigationData.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/4/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADNavigationData.h"

@implementation VADNavigationData

@synthesize currentTarget;
@synthesize currentRoute;

static VADNavigationData *instance =nil;

+(VADNavigationData *)instance
{
    @synchronized(self)
    {
        if(instance == nil)
        {
            
            instance = [VADNavigationData new];
        }
    }
    return instance;
}

@end
