//
//  VADSidebarRouteController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSidebarRouteController.h"
#import "VADSidebarViewController.h"

#import "VADAppDelegate.h"

@interface VADSidebarRouteController ()

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, weak) VADRoute * currentRoute;
@property (assign) BOOL ignoreWaypointChanges;

@end

@implementation VADSidebarRouteController

@synthesize managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute.waypoints"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    self.managedObjectContext = [[VADAppDelegate sharedDelegate] managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Route"];
    [request setReturnsObjectsAsFaults:YES];
    for (VADRoute *route in [self.managedObjectContext executeFetchRequest:request error:nil]){
        [[VADNavigationData instance] setCurrentRoute:route];
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_tblWaypoints reloadData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqual:@"currentRoute"]){
        self.currentRoute = [change objectForKey:NSKeyValueChangeNewKey];
        [_tblWaypoints reloadData];
    } else if ([keyPath isEqual:@"currentRoute.waypoints"] && !self.ignoreWaypointChanges){
        [_tblWaypoints reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapAddWaypoint:(UIButton *)sender {
    [((VADSidebarViewController*)self.parentViewController) gotoMapView];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.currentRoute.waypoints count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cellWaypoint";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil ){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    VADLandmark *waypoint = [self.currentRoute.waypoints objectAtIndex:indexPath.row];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    
    cell.textLabel.text = waypoint.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", waypoint.latitudeDMS, waypoint.longitudeDMS];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    nRemoveIndex = indexPath.row;
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Are you sure?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ( buttonIndex == 1 ) {
        [[VADAppDelegate sharedDelegate] removeWayPointAtIndex:nRemoveIndex];
        [_tblWaypoints reloadData];
    }
}

@end
