//
//  main.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VADAppDelegate class]));
    }
}
