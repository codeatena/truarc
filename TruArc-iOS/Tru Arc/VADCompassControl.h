//
//  VADCompassControl.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/15/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface VADCompassControl : UIControl

@property (retain) CLLocationManager *locationManager;
@property float compassHeading;

- (void) didUpdateHeading:(CLHeading *)newHeading;
- (void) didUpdateLocations:(NSArray *)locations;

@end
