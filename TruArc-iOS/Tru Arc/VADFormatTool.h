//
//  VADFormatTool.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/4/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

    /* Units */
#define UNITS_METRIC 1
#define UNITS_IMPERIAL 2
#define FEET_PER_METER 3.28084
#define METERS_PER_KILOMETER 1000.0
#define FEET_PER_MILE 5280.0

@interface VADFormatTool : NSObject

+ (NSString*) formatDistance:(CLLocationDistance)distance withUnit:(int)unit;
+ (NSString*) formatDistance:(CLLocationDistance)distance withUnit:(int)unit withPrecision:(int)precision;

@end
