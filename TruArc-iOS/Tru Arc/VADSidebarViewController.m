//
//  VADSidebarViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSidebarViewController.h"

@interface VADSidebarViewController ()

@property (nonatomic) dispatch_semaphore_t animation_semaphore;
@property (nonatomic) UIView* currentContainer;

@end

@implementation VADSidebarViewController

@synthesize collectionController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.animation_semaphore = dispatch_semaphore_create(1);
    self.currentContainer = self.educationContainer;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString: @"collection_embed"]) {
        self.collectionController = segue.destinationViewController;
    } else if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            if (![[self.revealViewController frontViewController] isKindOfClass:[dvc class]])
                [self.revealViewController setFrontViewController:dvc];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
    }
}

- (void)swapContainer:(UIView*)containerA with:(UIView*)containerB
{
    if (containerA == containerB) return;
        // If Animation Isn't Finished, Ignore It
    if (dispatch_semaphore_wait(self.animation_semaphore, DISPATCH_TIME_NOW)) return;
    
    [self.view bringSubviewToFront:containerB];
    [containerA setHidden:NO];
    CGRect frame = containerA.frame;
    containerB.frame = CGRectOffset(frame, 0.0, -CGRectGetHeight(frame));
    containerA.frame = frame;
    [containerB setHidden:NO];
    [UIView animateWithDuration:0.35
                     animations:^{
                         containerB.frame = frame;
                         containerA.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self.currentContainer = containerB;
                         [containerB setHidden:NO];
                         [containerA setHidden:YES];
                         containerB.frame = frame;
                         containerA.alpha = 1.0;
                            // Signal That the animation is done
                         dispatch_semaphore_signal(self.animation_semaphore);
                     }];
} 

- (void)resetToolbarStatus:(int)tagIndex {
    
    for ( int i = 100; i <= 106; i ++ ) {
        
        UIButton* btnItem = (UIButton*)[self.view viewWithTag:i];
        [btnItem setSelected:NO];
        
        if ( i == tagIndex )
            [btnItem setSelected:YES];
    }
}

- (IBAction)didTapConfigTool:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
}

- (IBAction)didTapSearchTool:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
}

- (IBAction)didTapCompassTool:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
}  

- (IBAction)didTapEducationTool:(UIButton *)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
    
    [self swapContainer:self.currentContainer with:self.educationContainer];
}

- (IBAction)didTapRouteTool:(UIButton *)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
    
    [self swapContainer:self.currentContainer with:self.routeContainer];
}

- (IBAction)didTapMapTool:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
}

- (IBAction)didTapListTool:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
}

- (IBAction)didTapSelectTarget:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
    
    [self swapContainer:self.currentContainer with:self.targetContainer];
}

- (IBAction)didTapDeclantation:(id)sender {
    [self resetToolbarStatus:((UIButton*)sender).tag];
    
    [self swapContainer:self.currentContainer with:self.toolsContainer];
}

- (void) gotoMapView {
    [self resetToolbarStatus:103];
    
    UIButton* btn = (UIButton*)[self.view viewWithTag:103];
    [self performSegueWithIdentifier:@"waypointmap" sender:btn];
}

- (void) gotoCompassView {
    [self resetToolbarStatus:101];
    
    UIButton* btn = (UIButton*)[self.view viewWithTag:101];
    [self performSegueWithIdentifier:@"compassview" sender:btn];
}

@end
