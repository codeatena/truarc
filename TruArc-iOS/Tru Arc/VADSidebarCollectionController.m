//
//  VADEducationViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/14/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSidebarCollectionController.h"

@interface VADSidebarCollectionController ()

@end

@implementation VADSidebarCollectionController

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.managedObjectContext = [[VADAppDelegate sharedDelegate] managedObjectContext];
    
    _objectChanges = [NSMutableArray array];
    _sectionChanges = [NSMutableArray array];
    
    // Instantiate Data
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:kCompassDataURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone: YES];
    });
    
    self.deleteMode = NO;
    
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		exit(-1);  // Fail
	}
}

- (void)fetchedData:(NSData *)responseData {
    if (responseData == nil) return;
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    if (error){
        return;
    }
    
    NSArray* data = [json objectForKey:@"data"];
    
    /* Synchronize Education Video Lists : Match By Video Checksum */
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EducationVideo" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (VADEducationVideo *dbVideo in results){
        bool exists = NO;
        for (NSArray *video in data) {
            NSString *checksum = [video valueForKey:@"videoChecksum"];
            if ([checksum isEqualToString:dbVideo.dataChecksum]){
                exists = YES;
                dbVideo.title = [video valueForKey:@"title"];
                dbVideo.imageFile = [video valueForKey:@"iconFile"];
                dbVideo.dataFile = [video valueForKey:@"videoFile"];
                dbVideo.dataSize = [video valueForKey:@"videoSize"];
            }
        }
        if (!exists){
            [dbVideo deleteLocalData];
            [managedObjectContext deleteObject:dbVideo];
        }
    }
    
    for (NSArray *video in data){
        bool exists = NO;
        for (VADEducationVideo *dbVideo in results){
            NSString *checksum = [video valueForKey:@"videoChecksum"];
            if ([checksum isEqualToString:dbVideo.dataChecksum]){
                exists = YES;
            }
        }
        if (!exists) {
            VADEducationVideo *newVideo = [NSEntityDescription
                                        insertNewObjectForEntityForName:@"EducationVideo"
                                        inManagedObjectContext:self.managedObjectContext];
            newVideo.title = [video valueForKey:@"title"];
            newVideo.imageFile = [video valueForKey:@"iconFile"];
            newVideo.dataChecksum = [video valueForKey:@"videoChecksum"];
            newVideo.dataFile = [video valueForKey:@"videoFile"];
            newVideo.dataSize = [video valueForKey:@"videoSize"];
        }
    }
    
    [managedObjectContext save:&error];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/** ----- Collection Data Methods ----- **/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return MAX([[_fetchedResultsController sections] count], 1);
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionReusableView*) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"educationViewHeader" forIndexPath:indexPath];
    return cell;
}

- (void) configureCell: (UICollectionViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    VADEducationVideo *educationVideo = [_fetchedResultsController objectAtIndexPath:indexPath];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
    
    UIView *downloadIcon = [cell viewWithTag:3];
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView*)[cell viewWithTag:4];
    UIProgressView *progressView = (UIProgressView*)[cell viewWithTag:6];
    
    [titleLabel setText: [educationVideo.title uppercaseString]];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:2];
    [educationVideo loadImageToView:imageView sourceURL:kCompassDataURL];
    
    progressView.hidden = YES;
    
    [UIView animateWithDuration:0.5 animations:^{
        [spinner stopAnimating];
        if ([educationVideo isDataAvailable]){
            downloadIcon.hidden = YES;
            imageView.alpha = 1.0;
        } else {
            downloadIcon.hidden = NO;
            imageView.alpha = 0.5;
        }
    }];
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"educationViewItem" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(CAAnimation*)rotationAnimation {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.values = @[@(-kWiggleRotateAngle), @(kWiggleRotateAngle)];
    
    animation.autoreverses = YES;
    animation.duration = [self randomizeInterval:kWiggleRotateDuration
                                    withVariance:kWiggleRotateDurationVariance];
    animation.repeatCount = HUGE_VALF;
    
    return animation;
}

-(CAAnimation*)bounceAnimation {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.values = @[@(kWiggleBounceY), @(0.0)];
    
    animation.autoreverses = YES;
    animation.duration = [self randomizeInterval:kWiggleBounceDuration
                                    withVariance:kWiggleBounceDurationVariance];
    animation.repeatCount = HUGE_VALF;
    
    return animation;
}

-(NSTimeInterval)randomizeInterval:(NSTimeInterval)interval withVariance:(double)variance {
    double random = (arc4random_uniform(1000) - 500.0) / 500.0;
    return interval + variance * random;
}

- (IBAction)didRecognizeLongPressGesture:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan && !self.deleteMode){
        self.deleteMode = YES;
        [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
    }
    
}

- (IBAction)didRecognizeTapGesture:(UITapGestureRecognizer *)sender {
    if (self.deleteMode){
        self.deleteMode = NO;
        [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    VADEducationVideo *educationVideo = [_fetchedResultsController objectAtIndexPath:indexPath];
    if ([educationVideo isDataAvailable]){
        if ([[educationVideo.localVideoPath pathExtension] isEqualToString:@"mp4"]){
            NSURL *videoURL = [NSURL fileURLWithPath:educationVideo.localVideoPath];
            MPMoviePlayerViewController *moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
            [self.revealViewController presentMoviePlayerViewControllerAnimated:moviePlayer];
        } else {
            VADLessonViewController *lessonView = [self.storyboard instantiateViewControllerWithIdentifier:@"viewLesson"];
            [lessonView setEducationImage:[UIImage imageWithContentsOfFile:[educationVideo localVideoPath]]];
            [self.revealViewController setFrontViewController:lessonView];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        }
    } else {
        
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        [(UIActivityIndicatorView*)[cell viewWithTag:4] startAnimating];
        [cell viewWithTag:3].hidden = YES;
        UIProgressView *progressView = (UIProgressView*)[cell viewWithTag:6];
        progressView.hidden = NO;
        progressView.progress = 0.0;
        
        [educationVideo downloadVideoWithBaseURL:kCompassDataURL
                                      onProgress:^(NSNumber* progress){
                                          UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
                                          [(UIActivityIndicatorView*)[cell viewWithTag:4] startAnimating];
                                          [cell viewWithTag:3].hidden = YES;
                                          UIProgressView *progressView = (UIProgressView*)[cell viewWithTag:6];
                                          progressView.hidden = NO;
                                          progressView.progress = [progress floatValue];
        }
                                      onComplete:^(NSError* error){
                                          if (error){
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Error"
                                                                                              message:@"Your lesson couldn't be downloaded. Please Try Again"
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:nil
                                                                                    otherButtonTitles:@"Okay", nil];
                                              [alert show];
                                          } else {
                                              [self configureCell:[self.collectionView cellForItemAtIndexPath:indexPath] atIndexPath:indexPath];
                                          }
        }];
    }
}

- (void)didRequestEducationTool
{
}

- (void)didRequestLocationTool
{
}

- (void)viewDidUnload {
    self.fetchedResultsController = nil;
}

- (void)dataDownloadProgressFor:(id)identifier atPercent:(NSNumber *)aPercent
{
    
    NSIndexPath *indexPath = (NSIndexPath*)identifier;
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    [(UIActivityIndicatorView*)[cell viewWithTag:4] startAnimating];
    [cell viewWithTag:3].hidden = YES;
    UIProgressView *progressView = (UIProgressView*)[cell viewWithTag:6];
    progressView.hidden = NO;
    progressView.progress = [aPercent floatValue];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EducationVideo" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

//
//  Created by Ash Furrow on 2012-09-11.
//  Copyright (c) 2012 Ash Furrow. All rights reserved.
//
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    NSMutableDictionary *change = [NSMutableDictionary new];
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_objectChanges addObject:change];
    
}

//
//  Created by Ash Furrow on 2012-09-11.
//  Copyright (c) 2012 Ash Furrow. All rights reserved.
//
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    NSMutableDictionary *change = [NSMutableDictionary new];
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            change[@(type)] = @(sectionIndex);
            break;
            
        case NSFetchedResultsChangeDelete:
            change[@(type)] = @(sectionIndex);
            break;
    }
    
    [_sectionChanges addObject:change];
}

//
//  Created by Ash Furrow on 2012-09-11.
//  Copyright (c) 2012 Ash Furrow. All rights reserved.
//
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    if ([_sectionChanges count] > 0)
    {
        [self.collectionView performBatchUpdates:^{
            
            for (NSDictionary *change in _sectionChanges)
            {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    switch (type)
                    {
                        case NSFetchedResultsChangeInsert:
                            [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                    }
                }];
            }
        } completion:nil];
    }
    
    if ([_objectChanges count] > 0 && [_sectionChanges count] == 0)
    {
        
        if ([self shouldReloadCollectionViewToPreventKnownIssue] || self.collectionView.window == nil) {
            // This is to prevent a bug in UICollectionView from occurring.
            // The bug presents itself when inserting the first object or deleting the last object in a collection view.
            // http://stackoverflow.com/questions/12611292/uicollectionview-assertion-failure
            // This code should be removed once the bug has been fixed, it is tracked in OpenRadar
            // http://openradar.appspot.com/12954582
            [self.collectionView reloadData];
            
        } else {
            
            [self.collectionView performBatchUpdates:^{
                
                for (NSDictionary *change in _objectChanges)
                {
                    [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                        
                        NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                        switch (type)
                        {
                            case NSFetchedResultsChangeInsert:
                                [self.collectionView insertItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeDelete:
                                [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeUpdate:
                                [self configureCell: [self.collectionView cellForItemAtIndexPath:obj] atIndexPath:obj];
//                                [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeMove:
                                [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                                break;
                        }
                    }];
                }
            } completion:nil];
        }
    }
    
    [_sectionChanges removeAllObjects];
    [_objectChanges removeAllObjects];
}

//
//  Created by Ash Furrow on 2012-09-11.
//  Copyright (c) 2012 Ash Furrow. All rights reserved.
//
- (BOOL)shouldReloadCollectionViewToPreventKnownIssue {
    __block BOOL shouldReload = NO;
    for (NSDictionary *change in self.objectChanges) {
        [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSFetchedResultsChangeType type = [key unsignedIntegerValue];
            NSIndexPath *indexPath = obj;
            switch (type) {
                case NSFetchedResultsChangeInsert:
                    if ([self.collectionView numberOfItemsInSection:indexPath.section] == 0) {
                        shouldReload = YES;
                    } else {
                        shouldReload = NO;
                    }
                    break;
                case NSFetchedResultsChangeDelete:
                    if ([self.collectionView numberOfItemsInSection:indexPath.section] == 1) {
                        shouldReload = YES;
                    } else {
                        shouldReload = NO;
                    }
                    break;
                case NSFetchedResultsChangeUpdate:
                    shouldReload = NO;
                    break;
                case NSFetchedResultsChangeMove:
                    shouldReload = NO;
                    break;
            }
        }];
    }
    
    return shouldReload;
}

@end


