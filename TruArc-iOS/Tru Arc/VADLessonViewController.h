//
//  VADLessonViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/6/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface VADLessonViewController : UIViewController

- (IBAction)didTapLogo:(UIButton *)sender;

- (void)setEducationImage:(UIImage *)newEducationImage;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@end
