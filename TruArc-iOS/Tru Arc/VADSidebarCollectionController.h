//
//  VADEducationViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/14/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define kCompassDataURL [NSURL URLWithString:@"http://resources.brunton.com/compass"]

#define kWiggleBounceY 4.0f
#define kWiggleBounceDuration 0.14
#define kWiggleBounceDurationVariance 0.025

#define kWiggleRotateAngle 0.04f
#define kWiggleRotateDuration 0.12
#define kWiggleRotateDurationVariance 0.025

#import "VADAppDelegate.h"
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "VADEducationVideo.h"
#import "VADLandmark.h"
#import "SWRevealViewController.h"
#import "VADLessonViewController.h"

@interface VADSidebarCollectionController : UICollectionViewController <NSFetchedResultsControllerDelegate, UICollectionViewDelegate>

- (IBAction)didRecognizeLongPressGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)didRecognizeTapGesture:(UITapGestureRecognizer *)sender;

- (void)didRequestEducationTool;
- (void)didRequestLocationTool;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSMutableArray *objectChanges;
@property (strong, nonatomic) NSMutableArray *sectionChanges;

@property BOOL deleteMode;

@end