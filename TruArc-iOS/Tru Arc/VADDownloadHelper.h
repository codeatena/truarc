//
//  VADDownloadHelper.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/5/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VADownloadHelperDelegate <NSObject>
@optional
- (void) dataDownloadStarted;
- (void) dataDownloadFailed:(NSString *) reason;
- (void) dataDownloadProgress:(NSNumber *) aPercent;
- (void) dataDownloadFinished;
- (void) dataDownloadsFinished;
@end

@interface VADDownloadHelper : NSObject <NSURLConnectionDataDelegate>
{
    NSOutputStream *outputStream;
    dispatch_queue_t downloadQueue;
    dispatch_semaphore_t downloadLimit;
    NSURL *url;
    NSURLConnection *urlConnection;
    NSURLResponse *urlResponse;
    BOOL isDownloading;
    long dataTotalBytes;
    long dataCurrentBytes;
    
    id <VADownloadHelperDelegate> delegate;
}

+(VADDownloadHelper*)instance;
+ (void) download:(NSURL *)url;
+ (void) download:(NSURL *)url destination:(NSString*)destination;
+ (void) download:(NSURL *)url destination:(NSString*)destination delegate:(id<VADownloadHelperDelegate>)newDelegate;
+ (void) cancel;

@property (retain) NSOutputStream *outputStream;
@property (assign) long dataTotalBytes;
@property (assign) long dataCurrentBytes;

@property (retain) NSURL *url;
@property (retain) dispatch_queue_t downloadQueue;
@property (retain) dispatch_semaphore_t downloadLimit;
@property (retain) NSURLConnection *urlConnection;
@property (retain) NSURLResponse *urlResponse;
@property (assign) BOOL isDownloading;
@property (retain) id<VADownloadHelperDelegate> delegate;

@end
