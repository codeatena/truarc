//
//  VADGlobal.m
//  Tru Arc
//
//  Created by MAC3 on 12/24/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADGlobal.h"

static VADGlobal* g_instance = nil;

@implementation VADGlobal

+ (VADGlobal*) getInstance {
    
    if ( !g_instance )
        g_instance = [[super alloc] init];
    
    return g_instance;
}

- (void) saveInfo:(id)objValue :(NSString*)strKey {
    NSUserDefaults* pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:objValue forKey:strKey];
    [pref synchronize];
}

- (id) loadInfo:(NSString*)strKey {
    NSUserDefaults* pref = [NSUserDefaults standardUserDefaults];
    return [pref objectForKey:strKey];
}

@end
