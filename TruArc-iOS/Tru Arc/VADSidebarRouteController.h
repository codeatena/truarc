//
//  VADSidebarRouteController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "VADAppDelegate.h"
#import "VADNavigationData.h"
#import "VADRoute.h"
#import "VADLandmark.h"
#import "VADSensorManager.h"

@interface VADSidebarRouteController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    int nRemoveIndex;
}


@property (weak, nonatomic) IBOutlet UITableView *tblWaypoints;

- (IBAction)didTapAddWaypoint:(UIButton *)sender;

@end
