//
//  VADCompassPanelView.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/3/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


#define FONT_HEADING            [UIFont fontWithName:@"UnitedSansCond-Light" size:60]
#define FONT_DIRECTION          [UIFont fontWithName:@"UnitedSansCond-Light" size:30]
#define SIZE_HEADINGBOXWIDTH    115.0
#define SIZE_LANDSCAPEHEIGHT    30.0
#define SIZE_HEADINGWIDTH       75.0

#define FONT_LABELS             [UIFont fontWithName:@"UnitedSansCond-Light" size:12]
#define FONT_DATA               [UIFont fontWithName:@"UnitedSansCond-Medium" size:18]


#define FONT_P_HEADING          [UIFont fontWithName:@"UnitedSansCond-Light" size:110]
#define FONT_P_COORDINATES      [UIFont fontWithName:@"UnitedSansCond-Light" size:20]
#define FONT_P_DIRECTION        [UIFont fontWithName:@"UnitedSansCond-Light" size:30]

#define FONT_P_LABELS           [UIFont fontWithName:@"UnitedSansCond-Light" size:14]
#define SIZE_P_LABELS           16.0
#define FONT_P_DATA             [UIFont fontWithName:@"UnitedSansCond-Light" size:20]
#define SIZE_P_DATA             24.0

#define FONT_P_TARGETLABEL      [UIFont fontWithName:@"UnitedSansExt-Light" size:14]
#define SIZE_P_TARGETLABEL      16.0
#define FONT_P_TARGETDATA       [UIFont fontWithName:@"UnitedSansExt-Light" size:22]
#define SIZE_P_TARGETDATA       28.0

#define SIZE_P_COORDHEIGHT      25.0
//#define

@interface VADCompassPanelView : UIView

- (void)setMinimal:(BOOL)newIsMinimal;
@property (assign) BOOL isMinimal;

- (void)setPortrait:(BOOL)newIsPortrait;
@property (nonatomic) BOOL isPortrait;

- (void)setHeading:(CLLocationDirection)newHeading;
- (void)errHeading;
@property (nonatomic) NSString* txtHeading;
@property (nonatomic) NSString* txtDirection;

- (void)setTarget:(NSString*)newTarget;
@property (nonatomic) NSString* txtTarget;

- (void)setTime:(NSString*)newTime;
@property (nonatomic) NSString* txtTime;

- (void)setAltitude:(NSString*)newAltitude;
@property (nonatomic) NSString* txtAltitude;

- (void)setLatitude:(NSString*)newLatitude;
@property (nonatomic) NSString* txtLatitude;
- (void)setLongitude:(NSString*)newLongitude;
@property (nonatomic) NSString* txtLongitude;

@end
