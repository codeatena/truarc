//
//  VADNavigationData.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/4/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "VADRoute.h"

@interface VADNavigationData : NSObject

@property CLLocationCoordinate2D currentTarget;
@property VADRoute * currentRoute;

+(VADNavigationData*)instance;

@end
