//
//  UILabel+UILabelFont.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/19/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UILabelFont)
@property (nonatomic, copy) NSString* customFont;
@end
