//
//  VADLandmark.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/21/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADLandmark.h"


@implementation VADLandmark

@dynamic index;
@dynamic created;
@dynamic latitude;
@dynamic longitude;
@dynamic title;

@dynamic route;

- (NSString*) latitudeDMS
{
    double latitude = [[self latitude] doubleValue];
    double DEG = trunc( latitude );
    double MIN = trunc( ( latitude - DEG ) * 60 );
    double SEC = ( latitude - DEG ) * 3600 - MIN * 60;
    char* DIR = (latitude >= 0)?"N":"S";
    return [NSString stringWithFormat: @"%s%1.0f° %1.0f' %1.3f\"", DIR, fabs(DEG), fabs(MIN), fabs(SEC)];
}

- (NSString*) longitudeDMS
{
    double longitude = [[self longitude] doubleValue];
    double DEG = trunc( longitude );
    double MIN = trunc( ( longitude - DEG ) * 60 );
    double SEC = ( longitude - DEG ) * 3600 - MIN * 60;
    char* DIR = (longitude >= 0)?"E":"W";
    return [NSString stringWithFormat: @"%s%1.0f° %1.0f' %1.3f\"", DIR, fabs(DEG), fabs(MIN), fabs(SEC)];
}

- (NSString *)subtitle
{
    return [NSString stringWithFormat:@"%@ %@",[self latitudeDMS], [self longitudeDMS]];
}

- (CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake([self.latitude doubleValue], [self.longitude doubleValue]);
}

- (BOOL) isSameObject:(VADLandmark*)second {
    
    if ( [self.title isEqualToString:second.title] && self.latitude.doubleValue == second.latitude.doubleValue && self.longitude.doubleValue == second.longitude.doubleValue )
        return YES;
    
    return NO;
}

@end
