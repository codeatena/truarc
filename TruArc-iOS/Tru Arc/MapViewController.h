//
//  MapViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SWRevealViewController.h"

@interface MapViewController : UIViewController

@property (retain) MKMapView *mapView;

- (IBAction)logoButtonTap:(UIButton *)sender;

@end
