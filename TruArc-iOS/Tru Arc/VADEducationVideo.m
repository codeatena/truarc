//
//  VADEducationVideo.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/21/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADEducationVideo.h"


@implementation VADEducationVideo

@dynamic title;
@dynamic imageFile;
@dynamic dataFile;
@dynamic dataChecksum;
@dynamic dataSize;
@dynamic downloaded;

@synthesize downloading;
@synthesize completionCallback;
@synthesize progressCallback;


+ (NSString*) directory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directoryPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"education_files"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return directoryPath;
}

- (void) loadImageToView: (UIImageView*)uiImageView sourceURL:(NSURL*)url
{
    NSString *filePath = [[VADEducationVideo directory] stringByAppendingPathComponent:self.imageFile];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        UIImage *image;
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            image = [UIImage imageWithContentsOfFile:filePath];
            if (image != nil){
                uiImageView.image = image;
            }
        }
        if (image == nil){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                NSError *error;
                
                NSData *imageData = [NSData dataWithContentsOfURL:[url URLByAppendingPathComponent:self.imageFile] options:NSDataReadingMappedIfSafe error:&error];
                if (!error){
                    [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
                    if (!error){
                        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
                        if (image != nil){
                            uiImageView.image = image;
                            [uiImageView setNeedsDisplay];
                        }
                    }
                }
                
            });
        }
    });
}

- (NSString*) localVideoPath
{
    return [[VADEducationVideo directory] stringByAppendingPathComponent:self.dataFile];
}

- (BOOL) doesDataExist
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self localVideoPath]];
}

- (BOOL) isDataAvailable
{
    return [self doesDataExist] && [self.downloaded boolValue] && !self.downloading;
}

- (void) deleteLocalData
{
    [[NSFileManager defaultManager] removeItemAtPath:[self localVideoPath] error:nil];
    self.downloaded = @NO;
}

- (BOOL) isDataValid
{
    if (![self doesDataExist]) return NO;
    
    NSInputStream *inputStream = [NSInputStream inputStreamWithFileAtPath:[self localVideoPath]];
    CC_MD5_CTX md5;
    CC_MD5_Init(&md5);
    
    NSInteger read;
    uint8_t byteBuffer[1024];
    
    [inputStream open];
    do {
        read = [inputStream read:byteBuffer maxLength:1024];
        if (read > 0) CC_MD5_Update(&md5, byteBuffer, read);
    } while (read > 0);
    [inputStream close];
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5);
    
    if (read < 0)
        return NO;
    else {
        NSString * digestStr = @"";
        for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
            digestStr = [digestStr stringByAppendingFormat:@"%02x", digest[i]];
        return [digestStr isEqualToString:self.dataChecksum];
    }
}

- (void) downloadVideoWithBaseURL:(NSURL*)url onComplete:(CompletionBlock)completed
{
    [self downloadVideoWithBaseURL:url onProgress:nil onComplete:completed];
}

- (void) downloadVideoWithBaseURL:(NSURL*)url onProgress:(ProgressBlock)progress onComplete:(CompletionBlock)completed
{
    if (!self.downloading){
        NSString *filePath = [[VADEducationVideo directory] stringByAppendingPathComponent:self.dataFile];
        completionCallback = completed;
        progressCallback = progress;
        self.downloading = YES;
        [VADDownloadHelper download:[url URLByAppendingPathComponent:self.dataFile] destination:filePath delegate:self];
    }
    [[self managedObjectContext] save:nil];
}

- (void)dataDownloadFinished
{
    self.downloading = NO;
    if (![self isDataValid]){
        self.downloaded = @NO;
        [self deleteLocalData];
        completionCallback = nil;
        progressCallback = nil;
        if (completionCallback) completionCallback([NSError errorWithDomain:@"com.VoltageAd.TruArc" code:42 userInfo:nil]);
        [[self managedObjectContext] save:nil];
    } else {
        self.downloaded = @YES;
        completionCallback = nil;
        progressCallback = nil;
        if (completionCallback) completionCallback(nil);
        [[self managedObjectContext] save:nil];
    }
}

- (void)dataDownloadProgress:(NSNumber *)aPercent
{
    self.downloading = YES;
    if (progressCallback) progressCallback(aPercent);
}

- (NSString*) calculateVideoChecksum
{
    if (![self doesDataExist]) return nil;
    
    return nil;
}

@end
