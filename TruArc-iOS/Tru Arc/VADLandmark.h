//
//  VADLandmark.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/21/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>
#import "VADRoute.h"

@interface VADLandmark : NSManagedObject <MKAnnotation>

@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, copy) NSString * title;

@property (nonatomic, retain) VADRoute * route;

- (NSString*) latitudeDMS;
- (NSString*) longitudeDMS;

- (BOOL) isSameObject:(VADLandmark*)second;

@end
