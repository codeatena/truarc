//
//  VADSidebarTargetController.h
//  Tru Arc
//
//  Created by MAC3 on 12/23/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VADAppDelegate.h"
#import "VADNavigationData.h"
#import "VADRoute.h"
#import "VADLandmark.h"
#import "VADSensorManager.h"

@interface VADSidebarTargetController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    int nSelectIndex;
}

@property (weak, nonatomic) IBOutlet UITableView *tblWaypoints;

@end  
