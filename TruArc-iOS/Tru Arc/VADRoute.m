//
//  VADRoute.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADRoute.h"
#import "VADLandmark.h"


@implementation VADRoute

@dynamic title;
@dynamic index;
@dynamic waypoints;

- (void)addWaypointsObject:(VADLandmark *)value
{
    NSMutableOrderedSet *waypoints = [NSMutableOrderedSet orderedSetWithOrderedSet:self.waypoints];
    [waypoints addObject:value];
    
    self.waypoints = waypoints;
}

- (void)removeObjectFromWaypointsAtIndex:(NSUInteger)idx
{
    NSMutableOrderedSet *waypoints = [NSMutableOrderedSet orderedSetWithOrderedSet:self.waypoints];
    [waypoints removeObjectAtIndex:idx];
    self.waypoints = waypoints;
}

- (MKPolyline*) makePolylineOverlay
{
    NSArray* array = [self.waypoints array];
    CLLocationCoordinate2D *plotLocation = malloc(sizeof(CLLocationCoordinate2D) * [array count]);
    int index = 0;
    for (VADLandmark *waypoint in array) {
        plotLocation[index++] = CLLocationCoordinate2DMake([[waypoint latitude] doubleValue], [[waypoint longitude] doubleValue]);
    }
    return [MKPolyline polylineWithCoordinates:plotLocation count:[array count]];
}

- (MKCoordinateRegion) getCoordinateRegion
{
    double minLat=90.0f, maxLat=-90.0f;
    double minLon=180.0f, maxLon=-180.0f;
    
    for (VADLandmark *wp in [self.waypoints array]) {
        if ( [wp.latitude doubleValue]  < minLat ) minLat = [wp.latitude doubleValue];
        if ( [wp.latitude doubleValue]  > maxLat ) maxLat = [wp.latitude doubleValue];
        if ( [wp.longitude doubleValue] < minLon ) minLon = [wp.longitude doubleValue];
        if ( [wp.longitude doubleValue] > maxLon ) maxLon = [wp.longitude doubleValue];
    }
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
    MKCoordinateSpan span = MKCoordinateSpanMake((maxLat-minLat), (maxLon-minLon));
    MKCoordinateRegion region = MKCoordinateRegionMake (center, span);
    
    return region;
}

@end
