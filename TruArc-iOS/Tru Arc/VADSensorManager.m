//
//  VADSensorManager.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/10/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSensorManager.h"

@interface VADSensorManager( )

@property (nonatomic, retain) NSMutableSet* locationObservers;
@property (nonatomic, retain) NSMutableSet* headingObservers;

@property (nonatomic, retain) NSMutableSet* motionObservers;
@property (nonatomic, retain) NSOperationQueue* motionQueue;

@end

@implementation VADSensorManager

static VADSensorManager *instance = nil;

+(VADSensorManager *)instance
{
    @synchronized(self){
        if(instance == nil){
            instance = [VADSensorManager new];
        }
    }
    return instance;
}

+(BOOL)locationAvailable
{
    return [CLLocationManager locationServicesEnabled];
}

- (CMMotionManager *)motionManager
{
    if (!motionManager) motionManager = [[CMMotionManager alloc] init];
    return motionManager;
}

- (CLLocationManager *)locationManager
{
    if (!locationManager) locationManager = [[CLLocationManager alloc] init];
    return locationManager;
}

-(id)init
{
    self = [super init];
    if (self) {
        [self locationManager].delegate = self;
        [self locationManager].desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        [self locationManager].distanceFilter = 5;
        
        self.motionQueue = [NSOperationQueue new];
        [self motionManager].deviceMotionUpdateInterval = 0.05;
    }
    return self;
}

- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;     // Make sure that if the device needs to, it will calibrate the compass.
                    // TODO: Only return YES if (headingError?) is large
}

- (void)registerForUpdatesTo:(id)destination selector:(SEL)selector name:(NSString *)name observers:(NSMutableSet*)observers
{
    if ([observers containsObject:destination]) return;
    [[NSNotificationCenter defaultCenter] addObserver:destination selector:selector
                                                 name:name object:self];
    [observers addObject:destination];
}

- (void)unregisterForUpdatesTo:(id)destination name:(NSString *)name observers:(NSMutableSet*)observers
{
    if (![observers containsObject:destination]) return;
    [[NSNotificationCenter defaultCenter] removeObserver:destination
                                                 name:name object:self];
    [observers removeObject:destination];
}

- (BOOL)registerForLocationUpdatesTo:(id)destination selector:(SEL)selector
{
    if (![CLLocationManager locationServicesEnabled]) return NO;
    if ([self.locationObservers count] <= 0)
        [self.locationManager startUpdatingLocation];
    [self registerForUpdatesTo:destination selector:selector name:@"sensor.updates.didUpdateLocations" observers:self.locationObservers];
    return YES;
}

- (void)unregisterForLocationUpdatesTo:(id)destination
{
    [self unregisterForUpdatesTo:destination name:@"sensor.updates.didUpdateLocations" observers:self.locationObservers];
    if ([self.locationObservers count] <= 0){
        [self.locationManager stopUpdatingLocation];
    }
}

- (void)restartLocationUpdates
{
    [self.locationManager stopUpdatingLocation];
    if ([self.locationObservers count] > 0){
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sensor.updates.didUpdateLocations"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:locations, @"location", manager, @"locationManager", nil]];
}

- (BOOL)registerForHeadingUpdatesTo:(id)destination selector:(SEL)selector
{
    if (![CLLocationManager headingAvailable]) return NO;
    if ([self.headingObservers count] <= 0)
        [self.locationManager startUpdatingHeading];
    [self registerForUpdatesTo:destination selector:selector name:@"sensor.updates.didUpdateHeading" observers:self.headingObservers];
    return YES;
}

- (void)unregisterForHeadingUpdatesTo:(id)destination
{
    [self unregisterForUpdatesTo:destination name:@"sensor.updates.didUpdateHeading" observers:self.headingObservers];
    if ([self.headingObservers count] <= 0){
        [self.locationManager stopUpdatingHeading];
    }
}

- (void)restartHeadingUpdates
{
    [self.locationManager stopUpdatingHeading];
    if ([self.locationObservers count] > 0){
        [self.locationManager startUpdatingHeading];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sensor.updates.didUpdateHeading"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:newHeading,@"heading",manager,@"locationManager", nil]];
}

- (BOOL)registerForDeviceMotionUpdatesTo:(id)destination selector:(SEL)selector
{
    if (![[self motionManager] isDeviceMotionAvailable]) return NO;
    if ([self.motionObservers count] <= 0){
        [self.motionManager startDeviceMotionUpdatesToQueue:self.motionQueue withHandler:^(CMDeviceMotion *motion, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self motionManager:self.motionManager didUpdateDeviceMotion:motion];
            });
        }];
//        NSLog(@"Motion Updates Started");
    }
    [self registerForUpdatesTo:destination selector:selector name:@"sensor.updates.didUpdateDeviceMotion" observers:self.motionObservers];
    return YES;
}

- (void)unregisterForDeviceMotionUpdatesTo:(id)destination
{
    [self unregisterForUpdatesTo:destination name:@"sensor.updates.didUpdateDeviceMotion" observers:self.motionObservers];
    if ([self.motionObservers count] <= 0){
        [self.motionManager stopDeviceMotionUpdates];
//        NSLog(@"Motion Updates Stopped");
    }
}

- (void)motionManager:(CMMotionManager *)manager didUpdateDeviceMotion:(CMDeviceMotion *)motion
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sensor.updates.didUpdateDeviceMotion"
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:motion,@"motion",manager,@"motionManager", nil]];
}

@end
