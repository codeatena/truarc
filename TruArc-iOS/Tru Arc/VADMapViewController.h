//
//  VADMapViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/3/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>
#import "VADAppDelegate.h"
#import "SWRevealViewController.h"
#import "VADNavigationData.h"
#import "VADFormatTool.h"
#import "VADRouteNode.h"
#import "VADRoute.h"

@interface VADMapViewController : UIViewController <MKMapViewDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UIAlertViewDelegate>

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

- (IBAction)tapLogoButton:(UIButton *)sender;
- (IBAction)didRecognizeLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;

@property (strong, nonatomic) IBOutlet MKMapView *mkMapView;
@property (strong, nonatomic) IBOutlet UILabel *lblDistance;

@property (nonatomic, strong) VADLandmark* selectedLandMark;

@end


