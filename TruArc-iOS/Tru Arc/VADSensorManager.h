//
//  VADSensorManager.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/10/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

@interface VADSensorManager : NSObject <CLLocationManagerDelegate> {
    CMMotionManager *motionManager;
    CLLocationManager *locationManager;
}

@property (readonly) CMMotionManager *motionManager;
@property (readonly) CLLocationManager *locationManager;

+(VADSensorManager*)instance;

- (BOOL)registerForLocationUpdatesTo:(id)destination selector:(SEL)selector;
- (void)unregisterForLocationUpdatesTo:(id)destination;
- (void)restartLocationUpdates;

- (BOOL)registerForHeadingUpdatesTo:(id)destination selector:(SEL)selector;
- (void)unregisterForHeadingUpdatesTo:(id)destination;
- (void)restartHeadingUpdates;

- (BOOL)registerForDeviceMotionUpdatesTo:(id)destination selector:(SEL)selector;
- (void)unregisterForDeviceMotionUpdatesTo:(id)destination;

@end
