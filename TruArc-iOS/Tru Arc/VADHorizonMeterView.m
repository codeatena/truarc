//
//  VADHorizonMeterView.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/6/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADHorizonMeterView.h"

@implementation VADHorizonMeterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)didMoveToSuperview
{
    CGRect bounds = self.layer.bounds;
    
    CAShapeLayer *crosshair = [CAShapeLayer layer];
    CGMutablePathRef crosshairPath = CGPathCreateMutable();
    CGPathMoveToPoint(crosshairPath, NULL, CGRectGetMidX(bounds), CGRectGetMinY(bounds));
    CGPathAddLineToPoint(crosshairPath, NULL, CGRectGetMidX(bounds), CGRectGetMaxY(bounds));
    CGPathMoveToPoint(crosshairPath, NULL, CGRectGetMinX(bounds), CGRectGetMidY(bounds));
    CGPathAddLineToPoint(crosshairPath, NULL, CGRectGetMaxX(bounds), CGRectGetMidY(bounds));
    
    crosshair.strokeColor = [UIColor whiteColor].CGColor;
    crosshair.lineWidth = 0.5;
    
    crosshair.path = crosshairPath;
    crosshair.frame = bounds;
    [self.layer addSublayer:crosshair];
    
    CAShapeLayer *horizon = [CAShapeLayer layer];
    CGRect horizonBounds = CGRectInset(bounds, CGRectGetWidth(bounds) / 3, CGRectGetHeight(bounds) / 3);
    CGMutablePathRef horizonPath = CGPathCreateMutable();
    CGPathMoveToPoint(horizonPath, NULL, CGRectGetWidth(horizonBounds) / 2, 0.0);
    CGPathAddLineToPoint(horizonPath, NULL, CGRectGetWidth(horizonBounds) / 2, CGRectGetHeight(horizonBounds));
    CGPathMoveToPoint(horizonPath, NULL, 0.0, CGRectGetHeight(horizonBounds) / 2);
    CGPathAddLineToPoint(horizonPath, NULL, CGRectGetWidth(horizonBounds), CGRectGetHeight(horizonBounds) / 2);
    
    horizon.strokeColor = [UIColor yellowColor].CGColor;
    horizon.lineWidth = 0.5;
    
    horizon.path = horizonPath;
    horizon.frame = horizonBounds;
    
    CAShapeLayer *horizonBall = [CAShapeLayer layer];
    CGRect horizonBallBounds = CGRectInset(bounds, CGRectGetWidth(bounds) / 8, CGRectGetHeight(bounds) / 8);
    CGMutablePathRef horizonBallPath = CGPathCreateMutable();
    CGPathAddEllipseInRect(horizonBallPath, NULL, horizonBallBounds);
    
    horizonBall.fillColor = [[UIColor alloc] initWithWhite:1.0 alpha:0.5].CGColor;
    
    horizonBall.path = horizonBallPath;
    horizonBall.frame = bounds;
    
    [horizonBall addSublayer:horizon];
    
    [self.layer addSublayer:horizonBall];
    
    self.horizonLayer = horizonBall;
}

-(void)setHorizon:(double)horizon withElevation:(double)elevation withArrow: (double) arrow
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    CATransform3D transform;
    
//    transform = CATransform3DMakeTranslation(0.0, elevation, 0.0);
//    transform = CATransform3DRotate(transform, horizon * M_PI / 180, 0.0, 0.0, 1.0);
    
    transform = CATransform3DMakeRotation(-horizon * M_PI / 180, 0.0, 0.0, 1.0);
    transform = CATransform3DRotate(transform, elevation * M_PI / 180, 1.0, 0.0, 0.0);
    transform = CATransform3DTranslate(transform, arrow, 0.0, -120.0);
    self.horizonLayer.anchorPointZ = 120.0;
    
//    NSLog(@"elevation = %f, arrow = %f", elevation, arrow);
    if (fabs(elevation) > 40 || fabs(arrow) > 120) {
        self.isHideArrow = NO;
        self.horizonLayer.hidden = YES;
    }
    else {
        self.isHideArrow = YES;
        self.horizonLayer.hidden = NO;
    }
    
    [self.horizonLayer setTransform:transform];
    
    //NSLog(@"currentPosition %f %f", self.horizonLayer.position.x, self.horizonLayer.position.y);
    
    [CATransaction commit];
}

- (void) moveHorizon: (CGPoint) point HIDDEN: (BOOL) isHidden
{
    self.horizonLayer.hidden = isHidden;
    [self.horizonLayer setPosition: point];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
