//
//  VADSidebarTargetController.m
//  Tru Arc
//
//  Created by MAC3 on 12/23/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADSidebarTargetController.h"

#import "VADGlobal.h"
#import "VADSidebarViewController.h"

@interface VADSidebarTargetController ()

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, weak) VADRoute * currentRoute;
@property (assign) BOOL ignoreWaypointChanges;

@end

@implementation VADSidebarTargetController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    [[VADNavigationData instance] addObserver:self
                                   forKeyPath:@"currentRoute.waypoints"
                                      options:NSKeyValueObservingOptionNew
                                      context:NULL];
    
    self.managedObjectContext = [[VADAppDelegate sharedDelegate] managedObjectContext];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Route"];
    [request setReturnsObjectsAsFaults:YES];
    for (VADRoute *route in [self.managedObjectContext executeFetchRequest:request error:nil]){
        [[VADNavigationData instance] setCurrentRoute:route];
    }
    
    BOOL bTargetSelected = [[[VADGlobal getInstance] loadInfo:IS_TARGET_SET] boolValue];
    if ( bTargetSelected ) {
        nSelectIndex = [[[VADGlobal getInstance] loadInfo:ACTIVE_TARGET] intValue];
        VADLandmark *waypoint = [self.currentRoute.waypoints objectAtIndex:nSelectIndex];
        [VADNavigationData instance].currentTarget = [waypoint coordinate];
    }
    else
        nSelectIndex = -1;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_tblWaypoints reloadData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqual:@"currentRoute"]){        
        self.currentRoute = [change objectForKey:NSKeyValueChangeNewKey];
        [_tblWaypoints reloadData];
    } else if ([keyPath isEqual:@"currentRoute.waypoints"] && !self.ignoreWaypointChanges){
        [_tblWaypoints reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.currentRoute.waypoints count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cellWaypoint";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil ){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    } 
    
    cell.backgroundColor = [UIColor clearColor];
    
    VADLandmark *waypoint = [self.currentRoute.waypoints objectAtIndex:indexPath.row];

    if ( nSelectIndex != indexPath.row ) {
        cell.textLabel.textColor = [UIColor whiteColor];
    }else {
        cell.textLabel.textColor = [UIColor yellowColor];
    }
    
    cell.detailTextLabel.textColor = [UIColor grayColor];
    
    cell.textLabel.text = waypoint.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", waypoint.latitudeDMS, waypoint.longitudeDMS];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    nSelectIndex = indexPath.row;
    [[VADGlobal getInstance] saveInfo:[NSString stringWithFormat:@"%d", nSelectIndex]: ACTIVE_TARGET];
    [[VADGlobal getInstance] saveInfo:@"1" :IS_TARGET_SET];
    
    VADLandmark *waypoint = [self.currentRoute.waypoints objectAtIndex:indexPath.row];
    [VADNavigationData instance].currentTarget = [waypoint coordinate];

    [_tblWaypoints reloadData];
    
    [((VADSidebarViewController*)self.parentViewController) gotoCompassView];
}

@end
