//
//  VADAngleMeterView.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/5/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface VADAngleMeterView : UIView

@property (nonatomic, strong) CALayer *degreesLayer;
@property (nonatomic, strong) CALayer *arrowLayer;
@property (nonatomic, assign) double degrees;

-(void)setDegrees:(double)newDegrees;

@end
