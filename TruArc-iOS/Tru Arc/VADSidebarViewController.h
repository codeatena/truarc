//
//  VADSidebarViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWRevealViewController.h"
#import "VADSidebarCollectionController.h"

@interface VADSidebarViewController : UIViewController

@property (strong, nonatomic) VADSidebarCollectionController *collectionController;

@property (strong, nonatomic) IBOutlet UIView *educationContainer;
@property (strong, nonatomic) IBOutlet UIView *toolsContainer;
@property (strong, nonatomic) IBOutlet UIView *routeContainer;
@property (strong, nonatomic) IBOutlet UIView *targetContainer;

- (IBAction)didTapConfigTool:(id)sender;
- (IBAction)didTapSearchTool:(id)sender;
- (IBAction)didTapCompassTool:(id)sender;

- (IBAction)didTapEducationTool:(UIButton *)sender;

- (IBAction)didTapRouteTool:(UIButton *)sender;
- (IBAction)didTapMapTool:(id)sender;
- (IBAction)didTapListTool:(id)sender;

- (IBAction)didTapSelectTarget:(id)sender;
- (IBAction)didTapDeclantation:(id)sender;

- (void) resetToolbarStatus:(int)tagIndex;

- (void) gotoMapView;
- (void) gotoCompassView;
@end
