//
//  VADHorizonMeterView.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/6/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VADHorizonMeterView : UIView

@property (nonatomic, strong) CAShapeLayer *horizonLayer;
@property (nonatomic) BOOL  isHideArrow;

-(void)setHorizon:(double)horizon withElevation:(double)elevation withArrow: (double) arrow;
- (void) moveHorizon: (CGPoint) point HIDDEN: (BOOL) isHidden;

@end
