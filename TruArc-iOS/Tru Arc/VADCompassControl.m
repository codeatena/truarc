//
//  VADCompassControl.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/15/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADCompassControl.h"

@implementation VADCompassControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    NSLog(@"initVADCompassControl");
    return self;
}

- (void) didUpdateHeading:(CLHeading *)newHeading
{
    if (self.compassHeading != self.locationManager.heading.magneticHeading){
        self.compassHeading = self.locationManager.heading.magneticHeading;
//        [self setNeedsDisplay];
    }
    NSLog(@"Updating Heading...");
}

- (void) didUpdateLocations:(NSArray *)locations
{
    
}

@end
