//
//  VADEducationVideo.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/21/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CommonCrypto/CommonDigest.h>

#import "VADDownloadHelper.h"

typedef void (^ProgressBlock)(NSNumber*);
typedef void (^CompletionBlock)(NSError*);

@interface VADEducationVideo : NSManagedObject <VADownloadHelperDelegate>{

}

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * imageFile;
@property (nonatomic, retain) NSString * dataFile;
@property (nonatomic, retain) NSString * dataChecksum;
@property (nonatomic, retain) NSNumber * dataSize;
@property (nonatomic, retain) NSNumber * downloaded;

@property (assign) BOOL downloading;

@property (readwrite, copy) CompletionBlock completionCallback;
@property (readwrite, copy) ProgressBlock progressCallback;

- (BOOL) isDataAvailable;
- (BOOL) doesDataExist;
- (NSString*) localVideoPath;
- (void) deleteLocalData;
- (void) loadImageToView: (UIImageView*)uiImageView sourceURL:(NSURL*)url;
- (void) downloadVideoWithBaseURL:(NSURL*)url onComplete:(void(^)(NSError*))completed;
- (void) downloadVideoWithBaseURL:(NSURL*)url onProgress:(ProgressBlock)progress onComplete:(CompletionBlock)completed;

@end
