//
//  MapViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    
    [self.mapView setMapType:MKMapTypeSatellite];
    [self.mapView setShowsUserLocation:YES];
    [self.view insertSubview:self.mapView atIndex:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logoButtonTap:(UIButton *)sender {
    [self.revealViewController revealToggle:sender];
}
@end
