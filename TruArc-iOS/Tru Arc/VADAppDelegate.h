//
//  VADAppDelegate.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/13/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

#import "VADSidebarCollectionController.h"
#import "VADNavigationData.h"
#import "VADFormatTool.h"

#import "VADRoute.h"

@interface VADAppDelegate : UIResponder <UIApplicationDelegate> {
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator; // test commit
}

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSMutableArray* arrWayPoints;
@property (nonatomic, strong) VADRoute* route;


- (NSURL *)applicationDocumentsDirectory;

+ (VADAppDelegate*)sharedDelegate;

- (void) insertNewWayPoint:(CLLocationCoordinate2D) coordinate;
- (void) removeWayPoint:(VADLandmark*)obj;
- (void) removeWayPointAtIndex:(int)index;
- (void) updateWayPointTitle:(VADLandmark*)obj :(NSString*)newTitle;
- (void) saveWayPoints;
- (void) loadWayPoints;

@end
