//
//  VADGlobal.h
//  Tru Arc
//
//  Created by MAC3 on 12/24/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "common.h"

@interface VADGlobal : NSObject

+ (VADGlobal*) getInstance;

- (void) saveInfo:(id)objValue :(NSString*)strKey;
- (id) loadInfo:(NSString*)strKey;

@end
