//
//  VADDownloadHelper.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/5/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADDownloadHelper.h"

@implementation VADDownloadHelper

@synthesize url;
@synthesize isDownloading;
@synthesize outputStream;
@synthesize downloadQueue;
@synthesize urlConnection;
@synthesize urlResponse;
@synthesize downloadLimit;
@synthesize dataCurrentBytes;
@synthesize dataTotalBytes;
@synthesize delegate;

static VADDownloadHelper *instance =nil;

- (void) start {
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:self.url];
    if (!urlRequest) {
//        NSLog(@"Error: Cannot Create Request From URL: %@", self.url);
        [self finish];
        return;
    }
    
    self.urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:NO];
    [self.urlConnection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    if (!self.urlConnection){
//        NSLog(@"Error: Cannot Create Connection From URL: %@", self.url);
        [self finish];
        [VADDownloadHelper cancel];
        return;
    }
    
//    NSLog(@"Prepared URL Connection To: %@", self.url);
    if (self.outputStream)
        [self.outputStream open];
    [self.urlConnection start];
    
    if ([delegate respondsToSelector:@selector(dataDownloadStarted)])
        [delegate dataDownloadStarted];
}

+(VADDownloadHelper *)instance
{
    @synchronized(self){
        if(instance == nil){
            instance = [VADDownloadHelper new];
            instance.downloadQueue = dispatch_queue_create( "com.Voltagead.Tru-Arc.download", NULL );
            instance.downloadLimit = dispatch_semaphore_create(1);
        }
    }
    return instance;
}

- (void) finish
{
    if (self.isDownloading) [self.urlConnection cancel];
    self.isDownloading = NO;
    [self cleanup];
    dispatch_semaphore_signal(self.downloadLimit);
}

- (void) cleanup
{
    self.outputStream = nil;
    self.urlResponse = nil;
    self.urlConnection = nil;
    self.url = nil;
    self.isDownloading = NO;
    self.dataTotalBytes = 0;
    self.dataCurrentBytes = 0;
}

+ (void) download:(NSURL *)url
{
    [VADDownloadHelper download:url destination:nil delegate:nil];
}

+ (void) download:(NSURL *)url destination:(NSString*)destination
{
    [VADDownloadHelper download:url destination:destination delegate:nil];
}

+ (void) download:(NSURL *)url destination:(NSString*)destination delegate:(id<VADownloadHelperDelegate>)newDelegate
{
    [VADDownloadHelper download:url destination:destination delegate:newDelegate identifier:nil];
}

+ (void) download:(NSURL *)url destination:(NSString*)destination delegate:(id<VADownloadHelperDelegate>)newDelegate identifier:(id)newIdentifier
{
    dispatch_async([VADDownloadHelper instance].downloadQueue, ^{
        
//        NSLog(@"Prepared Download From: %@", url);
        
            /* Wait Forever Until Download Helper Is Available */
        dispatch_semaphore_wait([VADDownloadHelper instance].downloadLimit, DISPATCH_TIME_FOREVER);
        
//        NSLog(@"Started Download From: %@", url);
        
        if ([VADDownloadHelper instance].isDownloading)
        {
//            NSLog(@"Error: Cannot start new download until current download finishes");
            return;
        }
        [VADDownloadHelper instance].delegate = newDelegate;
        [VADDownloadHelper instance].url = url;
        if (destination)
            [VADDownloadHelper instance].outputStream = [[NSOutputStream alloc] initToFileAtPath:destination append:NO];
        [[VADDownloadHelper instance] start];
        
    });
}

+ (void) cancel
{
    [[VADDownloadHelper instance] finish];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    NSLog(@"didReceiveResponse: %@", response);
    self.urlResponse = response;
    
    if ([response expectedContentLength] != NSURLResponseUnknownLength)
        self.dataTotalBytes = [response expectedContentLength];
    
    if ([response expectedContentLength] < 0){
        [self finish];
        return;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    float progress = -1.0;
    self.dataCurrentBytes += data.length;
    if (self.dataTotalBytes != NSURLResponseUnknownLength)
        progress = ((float)self.dataCurrentBytes / (float)self.dataTotalBytes);
    
    if (self.outputStream){
        NSUInteger left = [data length];
        NSUInteger nwr = 0;
        do {
            nwr = [self.outputStream write:[data bytes] maxLength:left];
            if (-1 == nwr) break;
            left -= nwr;
        } while (left > 0);
        if (left) {
//            NSLog(@"stream error: %@", [self.outputStream streamError]);
            [self finish];
        }
    }
    
    if (progress >= 0.0 && [delegate respondsToSelector:@selector(dataDownloadProgress:)])
        [delegate dataDownloadProgress:[NSNumber numberWithFloat:progress]];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"didFailWithError: %@", [error localizedDescription]);
    [self finish];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSLog(@"connectionDidFinishLoading");
    if (self.outputStream) [self.outputStream close];
    if ([delegate respondsToSelector:@selector(dataDownloadFinished)])
        [delegate dataDownloadFinished];
    [self finish];
}

@end
