//
//  VADSidebarDeclaintController.h
//  Tru Arc
//
//  Created by MAC3 on 12/24/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VADSidebarDeclaintController : UIViewController <UITextFieldDelegate>

- (IBAction)didChangeOption:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *tfEast;
@property (weak, nonatomic) IBOutlet UITextField *tfWest;
@property (weak, nonatomic) IBOutlet UISwitch *stAuto;

@end
  