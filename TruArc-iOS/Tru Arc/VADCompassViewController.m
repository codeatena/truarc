//
//  VADCompassViewController.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/25/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADCompassViewController.h"

#import "VADGlobal.h"

@interface VADCompassViewController ()

@end

@implementation VADCompassViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.viewConventional setBackgroundColor:[UIColor colorWithRed:50/255 green:50/255 blue:50/255 alpha:1.0]];
    
        /* Put Radius On Background Corners For Camera Buttons */
    [self.btnScreenshot.layer setCornerRadius:15.0];
    [self.lblHorizon.layer setCornerRadius:(self.lblHorizon.bounds.size.height / 2)];
    [self.lblElevation.layer setCornerRadius:(self.lblElevation.bounds.size.height / 2)];
    [self.lblBearing.layer setCornerRadius:(self.lblBearing.bounds.size.height / 2)];
    
        /* Add Cut Corners To GPS Coordinate Display */
    CGRect bounds = self.viewCoordinates.bounds;
    CGMutablePathRef mask = CGPathCreateMutable();
    CGPathMoveToPoint(mask, NULL, 0.0, 0.0);
    CGPathAddLineToPoint(mask, NULL, bounds.size.width, 0.0);
    CGPathAddLineToPoint(mask, NULL, bounds.size.width, bounds.size.height - 15.0);
    CGPathAddLineToPoint(mask, NULL, bounds.size.width - 15.0, bounds.size.height);
    CGPathAddLineToPoint(mask, NULL, 15.0, bounds.size.height);
    CGPathAddLineToPoint(mask, NULL, 0.0, bounds.size.height - 15.0);
    CGPathAddLineToPoint(mask, NULL, 0.0, 0.0);
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = mask;
    
    self.viewCoordinates.layer.mask = maskLayer;
    
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])){
        self.viewCoordinates.alpha = 0.0;
        [self cnstLandscapeLeft].constant = -[self view].bounds.size.width;
        [self cnstLandscapeToolbar].constant = [self view].bounds.size.width;
        [self cnstPortraitBottom].constant = 0.0;
        [[self viewLandscapePanel] setHidden:YES];
    } else {
        [self cnstPortraitBottom].constant = -120.0;
        [[self viewPortraitPanel] setHidden:NO];
    }
    
    [[self viewPortraitPanel] setPortrait:YES];
    
    [self.view setBackgroundColor:nil];
    
        /* Add Gesture Recognizer To Enable Swipping Over The Sidebar Menu */
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    
    
    northImageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, 280, 280)];
    northImageView.image = [UIImage imageNamed: @"compass.png"];
    
    arrowImageView = [[UIImageView alloc] initWithFrame: CGRectMake(45, 40, 10, 20)];
    arrowImageView.image = [UIImage imageNamed: @"arrow_north.png"];
    
    [self.viewHorizonMeter addSubview: arrowImageView];
//    testLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 300, 30)];
//    [testLabel setTextColor: [UIColor whiteColor]];
//    [self.view addSubview: testLabel];
    
    [self.viewConventional addSubview: northImageView];

    if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])) {
        northImageView.center = CGPointMake(self.view.center.x, self.view.center.y);
    } else {
        northImageView.center = CGPointMake(self.view.center.x, self.view.center.y - 60);
    }
    
    //float s = self.viewConventional.frame.size.height * 2;

    //[self.viewHorizonMeter addSubview: northImageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (float) getOrientationAngle:(UIInterfaceOrientation)orientation
{
    /* In order to prevent ambiguity, apply a small bias to prevent getting PI as an angle.
     Since PI and -PI are the same angle, the animation core doesn't know which way to animate. */
    float bias = M_PI / 2000.0;
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft: return M_PI / 2.0 - bias;
        case UIInterfaceOrientationPortrait: return 0.0 + bias;
        case UIInterfaceOrientationLandscapeRight: return -M_PI / 2.0 + bias;
        case UIInterfaceOrientationPortraitUpsideDown: return M_PI - bias;
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [[self viewPortraitPanel] setHidden: NO];
    [[self viewLandscapePanel] setHidden: YES];
    
        /* Calculate The Screen Angle Between The Current And New Device Orientations */
    self.cameraCompensationAngle = [self getOrientationAngle:toInterfaceOrientation] - [self getOrientationAngle:[self interfaceOrientation]];
    
    switch (toInterfaceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            [VADSensorManager instance].locationManager.headingOrientation = CLDeviceOrientationLandscapeRight;
            break;
        case UIInterfaceOrientationLandscapeRight:
            [VADSensorManager instance].locationManager.headingOrientation = CLDeviceOrientationLandscapeLeft;
            break;
        case UIInterfaceOrientationPortrait:
        default:
            [VADSensorManager instance].locationManager.headingOrientation = CLDeviceOrientationPortrait;
            break;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
            /* Show GPS Coordinates At Top Of Screen */
        [self viewCoordinates].alpha = 1.0;
        [self cnstLandscapeToolbar].constant = 115.0;
        [self cnstLandscapeLeft].constant = 0.0;
        [self cnstPortraitBottom].constant = -120.0;
        
        [self.view layoutIfNeeded];	
    } else {
            /* Hide GPS Coordinates From Top Of Screen */
        [self viewCoordinates].alpha = 0.0;
        [self cnstLandscapeLeft].constant = -[self view].bounds.size.width;
        [self cnstLandscapeToolbar].constant = [self view].bounds.size.width;
        [self cnstPortraitBottom].constant = 0.0;
        
        [self.view layoutIfNeeded];
    }
    
        /* Recenter Camera View In The New Orientation */
    [self cameraBackgroundView].center = CGPointMake(CGRectGetMidX([self view].bounds), CGRectGetMidY([self view].bounds));
        /* Apply The Compensation Angle To Make It Look Like The Camera View Doesn't Rotate */
    [self cameraBackgroundView].transform = CGAffineTransformMakeRotation(self.cameraCompensationAngle);
    
    [self.viewLandscapePanel setNeedsDisplay];
    [self.viewPortraitPanel setNeedsDisplay];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if (UIInterfaceOrientationIsLandscape([self interfaceOrientation])){
        northImageView.center = CGPointMake(self.view.center.x, self.view.center.y);
        [[self viewPortraitPanel] setHidden: YES];
    } else {
        northImageView.center = CGPointMake(self.view.center.x, self.view.center.y - 60);
        [[self viewLandscapePanel] setHidden: YES];
    }

        /* Update The Camera Orientation To Match The New Device Orientation */
    [[self cameraBackgroundView] updateOrientation:[self interfaceOrientation]];
        /* Reset The Camera View Transform */
    [[self cameraBackgroundView] setTransform:CGAffineTransformIdentity];
        /* Fit Camera View In New Space */
    [self cameraBackgroundView].frame = self.view.bounds;
}

- (void)startClock
{
    if (self.tmrClock)
        [self stopClock];
    self.tmrClock = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateClock) userInfo:nil repeats:YES];
}

-(void)stopClock
{
    [self.tmrClock invalidate];
    self.tmrClock = nil;
}

- (void)updateClock
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[NSDate date]];
    NSString *time = [NSString stringWithFormat:@"%02i:%02i:%02i", (int)components.hour, (int)components.minute, (int)components.second];
    [self.viewLandscapePanel setTime:time];
    [self.viewPortraitPanel setTime:time];
}

- (void)startHeadingUpdates
{
    if (![[VADSensorManager instance] registerForHeadingUpdatesTo:self selector:@selector(recievedHeadingUpdate:)]){
        [self.viewLandscapePanel errHeading];
        [self.viewPortraitPanel errHeading];
        [self.lblBearing setText:@"NO COMPASS AVAILABLE"];
    };
}

- (void)stopHeadingUpdates
{
    [[VADSensorManager instance] unregisterForHeadingUpdatesTo:self];
}

- (void)recievedHeadingUpdate:(NSNotification*)notification
{
//    CLLocationManager *manager = [notification.userInfo objectForKey:@"locationManager"];
    CLHeading *newHeading = [notification.userInfo objectForKey:@"heading"];
    
    [self.viewLandscapePanel setHeading: newHeading.magneticHeading];
    [self.viewPortraitPanel setHeading: newHeading.magneticHeading];
        /* Azimuth Calculations */
    
    double azimuth = newHeading.magneticHeading;
    
    //NSLog(@"heading = %f", azimuth);
    double quad = abs(abs(abs(azimuth - 180) - 90) - 90);
    double mils = (6400.0 / 360.0) * azimuth;
    
    NSString * quadrantStr;
    if      (359.0  < azimuth || azimuth <    1.0)
        quadrantStr = @"North";
    else if (  1.0 <= azimuth && azimuth <=  89.0)
        quadrantStr = [NSString stringWithFormat:@"N%2.0fE", quad];
    else if ( 89.0  < azimuth && azimuth <   91.0)
        quadrantStr = @"East";
    else if ( 91.0 <= azimuth && azimuth <= 179.0)
        quadrantStr = [NSString stringWithFormat:@"S%2.0fE", quad];
    else if (179.0  < azimuth && azimuth <  181.0)
        quadrantStr = @"South";
    else if (181.0 <= azimuth && azimuth <= 269.0)
        quadrantStr = [NSString stringWithFormat:@"S%2.0fW", quad];
    else if (269.0  < azimuth && azimuth <  271.0)
        quadrantStr = @"West";
    else if (271.0 <= azimuth && azimuth <= 359.0)
        quadrantStr = [NSString stringWithFormat:@"N%2.0fW", quad];
    else
        quadrantStr = @"ERR";   // This Technically Shouldn't Be Possible
    
    [self.lblBearing setText:[NSString stringWithFormat:@"%3.0f° %@ %4.0fmils", azimuth, quadrantStr, mils]];
    
    northImageView.transform = CGAffineTransformMakeRotation((-azimuth) * M_PI / 180);
    

    CLLocationCoordinate2D targetCoordinate = [VADNavigationData instance].currentTarget;
//    NSLog(@"Target Location %f %f", targetCoordinate.longitude, targetCoordinate.latitude);
    
    
//    while (YES) {
//        x = 50.0f + (targetCoordinate.longitude - currentCoordinate.longitude) * scaleX;
//        y = 50.0f - (targetCoordinate.latitude - currentCoordinate.latitude) * scaleY;
//        
//        if (x < -50.0f || x > 150.0f) {
//            scaleX /= 2.0f;
//            continue;
//        }
//        if (y < -50.0f || y > 150.0f) {
//            scaleY /= 2.0f;
//            continue;
//        }
//        
//        scale = (scaleX < scaleY) ? scaleX : scaleY;
//        x = 50.0f + (targetCoordinate.longitude - currentCoordinate.longitude) * scale;
//        y = 50.0f - (targetCoordinate.latitude - currentCoordinate.latitude) * scale;
//        break;
//    }
    //NSLog(@"position : %f, %f", x, y);

    double xs = currentCoordinate.longitude; double ys = currentCoordinate.latitude;
    double xe = targetCoordinate.longitude; double ye = targetCoordinate.latitude;
    
//    NSLog(@"currentCoordinate x, y = %f, %f", xs, ys);
//    NSLog(@"targetCoordinate x, y = %f, %f", xe, ye);

    double al = atan(2 * fabs(ye - ys) / fabs(xe - xs));
    
    

    
    if (isnan(al)) al = 0.0f;
    
    al = al * 180.0f / M_PI;
    //NSLog(@"abs al = %f", al);

    if (xe < xs && ye >= ys) {
        al = 180 - al;
    }
    if (xe <= xs && ye < ys) {
        al = 180 + al;
    }
    if (xe > xs && ye <= ys) {
        al = -al;
    }
    //NSLog(@"cur al = %f", al);
    //NSLog(@"azimuth = %f", azimuth);
    
    al += (azimuth);
    if (al > 360.0f) al -= 360;
    
    curArrow = al;
    arrowImageView.hidden = self.viewHorizonMeter.isHideArrow;

    
//    [testLabel setText: [NSString stringWithFormat: @"%f", al]];

    double xa = 50 + 40 * cos(al * M_PI / 180.0f);
    double ya = 50 - 40 * sin(al * M_PI / 180.0f);
    
    arrowImageView.center = CGPointMake(xa, ya);
    arrowImageView.transform = CGAffineTransformMakeRotation((-al) * M_PI / 180);

//    double x =
    
//    double al = (-azimuth) * M_PI / 180;
//    double r = 40.0f;
//    double x = 45.0f + r * cos(al); double y = 40.0f - r * sin(al);
//    
//    [northImageView setFrame: CGRectMake(x, y, 10, 20)];
//    self.viewPortraitPanel addSubview:
    
//    UIView* compassImage = [[self view] viewWithTag:9455];
}

- (void)startLocationUpdates
{
    if (![[VADSensorManager instance] registerForLocationUpdatesTo:self selector:@selector(recievedLocationUpdate:)]){
        [self.lblLCoord setText:@"ERR / ERR"];
    };
}

- (void)stopLocationUpdates
{
    [[VADSensorManager instance] unregisterForLocationUpdatesTo:self];
}

- (void)recievedLocationUpdate:(NSNotification*)notification
{
    CLLocationManager *manager = [notification.userInfo objectForKey:@"locationManager"];
    
    CLLocationDistance altitude = [manager location].altitude;
    NSString *strAltitude;
    
    int nDisplayMode = [[[VADGlobal getInstance] loadInfo:SETTING_MODE] intValue];
    if ( nDisplayMode == 0 )
        strAltitude = [NSString stringWithFormat:@"%.0fm", altitude];
    else
        strAltitude = [NSString stringWithFormat:@"%.0fft", altitude * FEET_PER_METER];
    
    [self.viewLandscapePanel setAltitude:strAltitude];
    [self.viewPortraitPanel setAltitude:strAltitude];
    
    CLLocationCoordinate2D coordinate = [manager location].coordinate;
    [self.lblLCoord setText:[NSString stringWithFormat:@"%+f° / %+f°", coordinate.latitude, coordinate.longitude]];
    
    double DEG = trunc( coordinate.latitude );
    double MIN = trunc( ( coordinate.latitude - DEG ) * 60 );
    double SEC = ( coordinate.latitude - DEG ) * 3600 - MIN * 60;
    char* DIR = (coordinate.latitude >= 0)?"N":"S";
    [self.viewPortraitPanel setLatitude:[NSString stringWithFormat: @"%s %1.0f° %1.0f' %1.3f\"", DIR, fabs(DEG), fabs(MIN), fabs(SEC)]];
    
    DEG = trunc( coordinate.longitude );
    MIN = trunc( ( coordinate.longitude - DEG ) * 60 );
    SEC = ( coordinate.longitude - DEG ) * 3600 - MIN * 60;
    DIR = (coordinate.longitude >= 0)?"E":"W";
    [self.viewPortraitPanel setLongitude:[NSString stringWithFormat: @"%s %1.0f° %1.0f' %1.3f\"", DIR, fabs(DEG), fabs(MIN), fabs(SEC)]];
    
    CLLocationCoordinate2D targetCoordinate = [VADNavigationData instance].currentTarget;
    currentCoordinate = coordinate;
    
    NSLog(@"Target Location: %f %f", targetCoordinate.latitude, targetCoordinate.longitude);
    NSLog(@"Current Location: %f %f", currentCoordinate.latitude, currentCoordinate.longitude);

    CLLocationDistance distance = [[manager location] distanceFromLocation:[[CLLocation alloc] initWithLatitude:targetCoordinate.latitude longitude:targetCoordinate.longitude]];
    //NSLog(@"distance : %f", distance);
    [self.viewLandscapePanel setTarget:[VADFormatTool formatDistance:distance withUnit:[[NSUserDefaults standardUserDefaults] integerForKey:@"compass.target.units"]]];
}

- (void)startAccelerometerUpdates
{
    [[VADSensorManager instance] registerForDeviceMotionUpdatesTo:self selector:@selector(recievedAccelerometerUpdate:)];
}

- (void)stopAccelerometerUpdates
{
    [[VADSensorManager instance] unregisterForDeviceMotionUpdatesTo:self];
}

- (void)recievedAccelerometerUpdate:(NSNotification*)notification
{
    CMMotionManager *manager = [[notification userInfo] objectForKey:@"motionManager"];
    CMDeviceMotion *data = [[notification userInfo] objectForKey:@"motion"];
    
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceUp && !self.orientationIsFaceUp){
        
        self.orientationIsFaceUp = YES;
        [self stopCamera];
        self.viewConventional.hidden = NO;
        [self.viewLandscapePanel setMinimal:YES];
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.viewInstruments.alpha = 0.0;
                             self.viewConventional.alpha = 1.0;
                             self.viewCoordinates.alpha = 0.0;
                             self.viewTools.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             self.viewInstruments.hidden = YES;
                         }];
        
        
    } else if ([[UIDevice currentDevice] orientation] != UIDeviceOrientationFaceUp && self.orientationIsFaceUp){
        self.orientationIsFaceUp = NO;
        self.viewInstruments.hidden = NO;
        [self startCamera];
        [self.viewLandscapePanel setMinimal:NO];
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.viewInstruments.alpha = 1.0;
                             self.viewConventional.alpha = 0.0;
                             if (UIInterfaceOrientationIsLandscape([self interfaceOrientation]))
                                 self.viewCoordinates.alpha = 1.0;
                             self.viewTools.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             self.viewConventional.hidden = YES;
                         }];
    }
    
    double pitch, roll;
    CMQuaternion quat = data.attitude.quaternion;
//    yaw = asin(2*(quat.x*quat.y + quat.w*quat.z)) * (180 / M_PI);
    roll = atan2(2*(quat.y*quat.w - quat.x*quat.z), 1 - 2*quat.y*quat.y - 2*quat.z*quat.z) * (180 / M_PI);
    pitch = atan2(2*(quat.x*quat.w + quat.y*quat.z), 1 - 2*quat.x*quat.x - 2*quat.z*quat.z) * (180 / M_PI);
    
    
        /* Smooth Attitude Data Using Kalman Filter */
//    self.lastYaw = [self kalmanFilter:yaw lastValue:self.lastYaw];
//    yaw = self.lastYaw;
    self.lastRoll = [self kalmanFilter:roll lastValue:self.lastRoll];
    self.lastPitch = [self kalmanFilter:pitch lastValue:self.lastPitch];
    
    float horizon, elevation;
    horizon = atan2(data.gravity.x, data.gravity.y) * (180 / M_PI);
    
    if (horizon > 0)
        horizon = 180 - horizon;
    else
        horizon = -180 - horizon;
    
//    elevation = asin(data.gravity.z) * (180 / M_PI);
    elevation = atan2(sqrt(data.gravity.x * data.gravity.x + data.gravity.y * data.gravity.y), data.gravity.z) * (180 / M_PI);
    
    
    if ( [self interfaceOrientation] == UIDeviceOrientationPortrait){
//        elevation = pitch - 90;
        if (elevation < -180.0) elevation = elevation + 360;
        if (data.gravity.y < 0) elevation = -elevation;
    } else if ( [self interfaceOrientation] == UIDeviceOrientationLandscapeLeft ){
        horizon = horizon + 90;
        if (horizon > 180.0) horizon = - (360 - horizon);
//        elevation = -(roll + 90);
        if (data.gravity.x < 0) elevation = -elevation;
    } else if ( [self interfaceOrientation] == UIDeviceOrientationLandscapeRight ){
        horizon = horizon - 90;
        if (horizon < -180.0) horizon = horizon + 360;
//        elevation = roll - 90;
        elevation = -elevation;
        if (data.gravity.x < 0) elevation = -elevation;
    } else { // Uhhh.... We shouldn't  be here....
        horizon = 0.0;
        elevation = 0.0;
    }
    
    elevation += 90;
    if (elevation > 180.0) elevation = - (360 - elevation);
    
        /* Smooth Horizon Angle Using Kalman Filter */
    self.lastHorizon = [self kalmanFilter:horizon lastValue:self.lastHorizon];
    horizon = self.lastHorizon;
    
        /* Eliminate Negative Zeros ... */
//    if (horizon == 0.0) horizon = 0.0;
//    if (elevation == 0.0) elevation = 0.0;
    
    [self.viewHorizon setDegrees:horizon];
    [self.viewElevation setDegrees:elevation];
    
    
    [self.viewHorizonMeter setHorizon:horizon withElevation:elevation withArrow:(90.0f - curArrow)];
    
    //[testLabel setText: [NSString stringWithFormat: @"x=%f y=%f, z=%f", data.gravity.x, data.gravity.y, data.gravity.z]];
    [self.lblHorizon setText:[NSString stringWithFormat:@"%+03.1f°",  horizon]];
    [self.lblElevation setText:[NSString stringWithFormat:@"%+03.1f°", elevation]];

}

- (double)kalmanFilter:(double)newValue lastValue:(double)lastValue
{
    if (lastValue == 0) {
        lastValue = newValue;
    }
    
    // kalman filtering
    static float q = 0.1;   // process noise
    static float r = 0.8;   // sensor noise
    static float p = 0.3;   // estimated error
    static float k = 0.5;   // kalman filter gain
    
    double x = lastValue;
    p = p + q;
    k = p / (p + r);
    x = x + k*(newValue - x);
    p = (1 - k)*p;
    return x;
}

- (void)startCamera
{
    if (!self.cameraBackgroundView){
            /* Allocate and Initialize Camera Background View */
        [self setCameraBackgroundView:[[VADCameraView alloc] initWithFrame:self.view.bounds]];
        [self cameraBackgroundView].center = CGPointMake(CGRectGetMidX([self view].bounds), CGRectGetMidY([self view].bounds));
        [self.view insertSubview:self.cameraBackgroundView atIndex:0];
        [[self cameraBackgroundView] updateOrientation:[self interfaceOrientation]];
    } else {
        [[self cameraBackgroundView] startCamera];
    }
}

- (void)stopCamera
{
    [[self cameraBackgroundView] stopCamera];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startClock];
    [self startAccelerometerUpdates];
    [self startHeadingUpdates];
    [self startLocationUpdates];
    [self startCamera];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
        /* Stop All The Updates To Reduce CPU Load */
    [self stopAccelerometerUpdates];
    [self stopClock];
    [self stopHeadingUpdates];
    [self stopLocationUpdates];
    [self stopCamera];
}

- (IBAction)tapScreenshot:(UIButton *)sender {
    
    _bCaptureSuccess = YES;
    
    [self.cameraBackgroundView capture:^(UIImage* cameraImage){
        
        if ( cameraImage == nil ) {
            _bCaptureSuccess = NO;
        } else {
            _bCaptureSuccess = YES;
            
            float scale = fmax(self.view.bounds.size.width / cameraImage.size.width, self.view.bounds.size.height / cameraImage.size.height);
            float x = (self.view.bounds.size.width - (cameraImage.size.width * scale)) / 2;
            float y = (self.view.bounds.size.height - (cameraImage.size.height * scale)) / 2;
            CGRect imageRect = CGRectMake(x, y, cameraImage.size.width * scale, cameraImage.size.height * scale);
            
            UIGraphicsBeginImageContextWithOptions(self.view.frame.size, NO, 0.0);
            [cameraImage drawInRect:imageRect];
            [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIImageView *imagePreview = [[UIImageView alloc] initWithFrame:self.view.bounds];
            [imagePreview setImage:viewImage];
            [self.view addSubview:imagePreview];
            
            NSArray *activityItems = [NSArray arrayWithObjects:@"", viewImage, nil];
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact ];
            
            [self presentViewController:activityViewController animated:YES completion:nil];
            
            self.view.userInteractionEnabled = NO;
            [UIView animateWithDuration:0.5 animations:^{
                imagePreview.center = self.btnScreenshot.center;
                imagePreview.transform = CGAffineTransformMakeScale(0.0, 0.0);
            } completion:^(BOOL finished){
                self.view.userInteractionEnabled = YES;
                [imagePreview removeFromSuperview];
            }];
        }
    }];
    
    
    if ( !_bCaptureSuccess ) {
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Oops"
                                                        message:@"This device doesn't support camera."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)tapLogo:(UIButton *)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)tapTools:(UIButton *)sender {
//    [((VADSidebarViewController*)self.revealViewController.rearViewController) didTapLocationTool:nil];
    [self.revealViewController revealToggle:sender];
}


@end
