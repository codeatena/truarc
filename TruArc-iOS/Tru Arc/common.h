//
//  common.h
//  Tru Arc
//
//  Created by MAC3 on 12/24/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#ifndef Tru_Arc_common_h
#define Tru_Arc_common_h

#define SETTING_MODE    @"setting_mode"

#define DECLAINT_AUTO   @"declaint_auto"
#define DEC_EAST        @"dec_east"
#define DEC_WEST        @"dec_west"

#define INIT_LOADING    @"init_loading"

#define ACTIVE_TARGET   @"active_target"
#define IS_TARGET_SET   @"is_target_set"

#endif
