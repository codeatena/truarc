//
//  VADRouteNode.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/4/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "VADRouteNode.h"

@implementation VADRouteNode

@synthesize coordinate;

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    coordinate = newCoordinate;
}

@end
