//
//  UILabel+UILabelFont.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/19/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "UILabel+UILabelFont.h"

@implementation UILabel (UILabelFont)

- (NSString *)customFont {
    return self.customFont;
}

- (void)setCustomFont:(NSString *)newCustomFont {
    NSArray* values = [newCustomFont componentsSeparatedByString:@":"];
    if (values.count > 1)
        self.font = [UIFont fontWithName:[values objectAtIndex:0] size:[[values objectAtIndex:1] floatValue]];
    else
        self.font = [UIFont fontWithName:[values objectAtIndex:0] size:self.font.pointSize];
}

@end
