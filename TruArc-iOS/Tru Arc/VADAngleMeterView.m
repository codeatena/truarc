//
//  VADAngleMeterView.m
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/5/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import "VADAngleMeterView.h"

@implementation VADAngleMeterView

@synthesize degrees;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)didMoveToSuperview
{
    CGRect bounds = self.layer.bounds;
    
    self.degreesLayer = [CALayer layer];
    UIImage *degreesImage = [UIImage imageNamed:@"brunton-tru-arc-degrees-transparency.png"];
    self.degreesLayer.contents = (id)degreesImage.CGImage;
    self.degreesLayer.frame = CGRectMake(bounds.origin.x + 2.0, CGRectGetMidY(bounds) - degreesImage.size.height / 2 + 1, degreesImage.size.width, degreesImage.size.height);
    self.degreesLayer.masksToBounds = YES;
    [self.layer addSublayer:self.degreesLayer];
    
    self.arrowLayer = [CALayer layer];
    UIImage *arrowImage = [UIImage imageNamed:@"yellow-arrows.png"];
    self.arrowLayer.contents = (id)arrowImage.CGImage;
    self.arrowLayer.frame = CGRectMake(bounds.origin.x, CGRectGetMidY(bounds) - arrowImage.size.height / 2, arrowImage.size.width, arrowImage.size.height);
    self.arrowLayer.masksToBounds = YES;
    [self.layer addSublayer:self.arrowLayer];
}

-(void)setDegrees:(double)newDegrees
{
    degrees = newDegrees;
    CGRect bounds = self.layer.bounds;
    CGRect frame = self.degreesLayer.frame;
    
    double offset = (newDegrees * 3.0);
//    self.degreesLayer.position = CGPointMake(bounds.origin.x + 2.0, y);
    [self.degreesLayer removeAnimationForKey:@"frame"];
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.degreesLayer.frame = CGRectMake(bounds.origin.x + 2.0, CGRectGetMidY(bounds) - frame.size.height / 2 + 1 + offset, frame.size.width, frame.size.height);
    [CATransaction commit];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    UIImage *imgDegrees = [UIImage imageNamed:@"brunton-tru-arc-degrees-transparency.png"];
//    CGRect imgDegreesRect = CGRectMake(2.0, rect.size.height / 2 - imgDegrees.size.height / 2 + 1.0, imgDegrees.size.width, imgDegrees.size.height);
//    [imgDegrees drawInRect:imgDegreesRect];
//    
//    UIImage *image = [UIImage imageNamed:@"yellow-arrows.png"];
//    CGRect imageRect = CGRectMake(0.0, rect.size.height / 2 - image.size.height / 2, image.size.width, image.size.height);
//    [image drawInRect:imageRect];
//}

@end
