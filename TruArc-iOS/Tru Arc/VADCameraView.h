//
//  VADCameraView.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 11/20/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VADCameraView : UIView

@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (retain) AVCaptureStillImageOutput *stillImageOutput;

- (void)updateOrientation:(UIInterfaceOrientation)orientation;
- (void)capture:(void(^)(UIImage* image)) callback;

- (void)startCamera;
- (void)stopCamera;

@end
