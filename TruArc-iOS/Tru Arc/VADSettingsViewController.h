//
//  VADSettingsViewController.h
//  Tru Arc
//
//  Created by VOLTAGE AIR on 12/12/13.
//  Copyright (c) 2013 VoltageAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VADSettingsViewController : UITableViewController {
    NSIndexPath* selectedIndexPath;
}

- (IBAction)tapDone:(UIBarButtonItem *)sender;

@end
